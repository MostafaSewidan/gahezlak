<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


////////////////////////////////
// confirm Auth

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::post('send-pin-code', 'AuthController@sendPinCode');
Route::post('reset-password', 'AuthController@resetPassword');

Route::group(['middleware' => ['auth:api_delivery']], function () {

    ////////////////////////////////
    //  Auth transactions

    Route::get('list-profile', 'AuthController@showProfile');
    Route::post('update-profile', 'AuthController@updateProfile');
    Route::post('logout', 'AuthController@logOut');
    Route::post('update-location', 'AuthController@updateLocation');

    ////////////////////////////////

    ////////////////////////////////
    //  transactions

    Route::post('add-review', 'TransactionController@addReview');
    Route::get('list-reviews', 'TransactionController@listReviews');

    ////////////////////////////////

    ////////////////////////////////
    //  points transactions

    Route::get('list-points', 'PointsController@listPoints');
    Route::post('change-points', 'PointsController@changePoints');

    ////////////////////////////////


    Route::group(['prefix' => 'orders'], function () {

        ////////////////////////////////
        //  order transactions

        Route::get('list', 'OrderController@listOrders');
        Route::post('make-offer', 'OrderController@offerOrder');

        ////////////////////////////////
        ///
    });
});
////////////////////////////////


