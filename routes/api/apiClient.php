<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



////////////////////////////////
// confirm Auth

    Route::post('login', 'AuthController@login');
    Route::post('send-pin-code', 'AuthController@clientSendPinCode');
    Route::post('reset-password', 'AuthController@resetPassword');
    Route::post('confirm-account', 'AuthController@confirmAccount');

    Route::group(['middleware' => ['auth:api_client', 'client-check']], function () {

        ////////////////////////////////
        //  Auth transactions
        Route::post('accept-condition', 'AuthController@acceptCondition');
        Route::get('client-profile', 'AuthController@showProfile');
        Route::get('client-wallet-balance', 'AuthController@showWalletBalance');
        Route::post('update-profile', 'AuthController@updateProfile');
        Route::post('logout', 'AuthController@logOut');
        //Route::post('make-service', 'AuthController@makeService');
        ////////////////////////////////

        ////////////////////////////////
        //  client transactions

        Route::post('add-review', 'TransactionController@addReviews');
        Route::post('add-address', 'TransactionController@addAddress');
        Route::post('remove-address', 'TransactionController@removeAddress');
        Route::get('show-addresses', 'TransactionController@showAddresses');

        ////////////////////////////////

        ////////////////////////////////
        ///
        // notifications

        Route::get('notifications', 'TransactionController@notifications');
        Route::post('delete-notification', 'TransactionController@deleteNotification');
        Route::post('read-notification', 'TransactionController@readNotification');

        ////////////////////////////////
        ///
        // Reservations

        Route::post('make-reservation', 'HomeController@makeReservation');
        ////////////////////

        // Write order
        Route::get('my-tours', 'HomeController@myTours');
        ///////////////////////////////

        ////////////////////////////////

        // order transactions

        Route::post('test-coupon', 'OrderController@checkCoupon');
        Route::post('store-order', 'OrderController@storeOrder');
        Route::post('personal-shopper-order', 'OrderController@personalShopperOrder');
        Route::post('ask-service-order', 'OrderController@askService');
        Route::post('send-gifts-order', 'OrderController@sendGifts');
        Route::post('send-shipments-in-my-area-order', 'OrderController@sendShipmentsInMyArea');
        Route::post('send-shipments-with-travelers-order', 'OrderController@sendShipmentWithTravelers');
        Route::get('list-orders', 'OrderController@listOrders');
        /////////////////////////////////////////


        Route::post('contact-us', 'TransactionController@contactUs');

    });
////////////////////////////////


////////////////////////////////
// general apis
    Route::get('settings', 'HomeController@settings');
    Route::get('home', 'HomeController@home');
    Route::get('countries', 'HomeController@countries');
    Route::get('stores', 'HomeController@stores');
    Route::get('proposed-stores', 'HomeController@proposedStores');
    Route::get('payment-methods', 'HomeController@PaymentMethods');

////////////////////////////////
