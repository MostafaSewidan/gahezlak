<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(countries::class);
        $this->call(cities::class);
        $this->call(regions::class);
        $this->call(categories::class);
        $this->call(services::class);
        $this->call(stores::class);
        $this->call(settings::class);
        $this->call(coupons::class);
    }
}
