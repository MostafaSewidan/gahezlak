<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class cities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Country::all() as $country) {
            for($i = 1; $i <= 5; $i++)
            {
                $country->cities()->create(['name' =>  $i.' مثال لإسم مدينة ']);
            }
        }
    }
}
