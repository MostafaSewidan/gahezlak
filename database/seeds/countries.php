<?php

use Illuminate\Database\Seeder;
use \App\Models\Country;
class countries extends Seeder
{
   private $valuse = [
       'المنطقة الشمالية',
       'المنطقة الجنوبية',
       'المنطقة الشرقية',
       'المنطقة الغربية',
   ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->valuse as $country)
        {
            Country::create(['name' => $country]);
        }
    }
}
