<?php

use App\Models\City;
use Illuminate\Database\Seeder;

class regions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (City::all() as $city) {
            for($i = 1; $i <= 5; $i++)
            {
                \App\Models\Region::create(['name' => $i.' مثال لإسم حي ' , 'city_id' => $city->id]);
            }
        }
    }
}
