<?php

use Illuminate\Database\Seeder;

class categories extends Seeder
{
    private $valuse = [
        'مطاعم',
        'كافيهات',
        'صحي',
        'سوبرماركت',
        'بوفيه',
        'مخبوزات وحلويات',
        'صيدليات',
        'الأسر المنتجة',
    ];

    private $photos = [
        'res.png',
        'cafe.png',
        's7e.png',
        'market.png',
        'bofe.png',
        'ma5bozat.png',
        'pharm.png',
        'osar.png',
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counter = 0;
        foreach ($this->valuse as $value)
        {
            $cat = \App\Models\Category::create(['name' => $value]);
            $cat->photos()->create([
                'path' => 'photos/'.$this->photos[$counter],
                'type' => 'image',
                'usage' => null
            ]);
            $counter++;
        }
    }
}
