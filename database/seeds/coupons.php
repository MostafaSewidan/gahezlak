<?php

use Illuminate\Database\Seeder;

class coupons extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Coupon::create([
            'code' => '123',
            'use_frequency' => '10000',
            'expired_date' => '2020-12-20 13:11:08',
            'start_date' => '2020-09-01 13:11:08',
            'type' => 'fixed',
            'amount' => '30',
            'max_discount' => 80,
        ]);
    }
}
