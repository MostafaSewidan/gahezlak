<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class services extends Seeder
{
    private $valus = [
        'personal_shopper' =>[
            'title' => 'المتسوق الشخصي',
            'search_area' => 10,
            'photo' => 'shop.png',
        ],
        'send_shipments' => [
            'title' => 'ارسل غرض',
            'search_area' => 10,
            'photo' => 'send.png',
        ],
        'ask_service' => [
            'title' => 'اطلب خدمة',
            'search_area' => 10,
            'photo' => 'ask_service.png',
        ],
        'send_gifts' =>[
            'title' => 'وش حاب تهدي حبايبك',
            'search_area' => 10,
            'photo' => 'gift.png',
        ],
    ];


    private $askSubService = [
        'fuel_filling' => [
            'title' => 'تعبئة وقود',
            'search_area' => 0
        ],
        'open_car' => [
            'title' => 'فتح سيارة',
            'search_area' => 0
        ],
        'tire' => [
            'title' => 'بنشر كفر',
            'search_area' => 0
        ],
        'battery|_repair' => [
            'title' => 'إصلاح بطارية',
            'search_area' => 0
        ],
        'cary' => [
            'title' => 'سطحة',
            'search_area' => 0
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->valus as $key => $val)
        {
            $service = Service::create(['title' => $val['title'] , 'search_area' => $val['search_area'] , 'type' => $key]);

            $service->photo()->create([
                'path' => 'photos/'.$val['photo'],
                'type' => 'image',
                'usage' => null
            ]);

        }

        $parent = Service::where('type' , 'send_shipments')->first();
        $parent->children()->create(['title' => 'في نطاق منطقتك' , 'search_area' => 10 , 'type' => 'in_my_area']);
        $parent->children()->create(['title' => 'رحلات المسفرين بالسيارة' , 'search_area' => 10 , 'type' => 'travelers']);


        $askService = Service::where('type' , 'ask_service')->first();

        foreach($this->askSubService as $key => $val)
        {
            $ser = $askService->children()->create(['title' => $val['title'], 'search_area' => $val['search_area'] , 'type' => $key]);
            $ser->descriptions()->create(['description' => 'داخل المدينة (غير شامل سعر الوقود) قيمة الخدمة 25 ر.سعودي']);

        }

    }
}
