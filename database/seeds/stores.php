<?php

use Illuminate\Database\Seeder;

class stores extends Seeder
{
   private $lat = [31.0251571,31.0250835,31.0391861,31.0369938];
   private $long = [31.3660343,31.3514646,31.3767203,31.3629108];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Category::all() as $cat)
        {
            for ($i = 0; $i <= 6; $i++)
            {
                $store = \App\Models\Store::create([
                    'phone' => '05015151515',
                    'name' => 'محل ',
                    'proposed' => rand(0,1),
                ]);

                $rand = rand(0,3);

                $store->addresses()->create([
                    'title' => 'عنوان الفرع الرئيسي',
                    'type' => 'main',
                    'latitude' => $this->lat[$rand],
                    'longitude' => $this->long[$rand],
                ]);
                $cat->stores()->attach([$store->id]);
            }
        }
    }
}
