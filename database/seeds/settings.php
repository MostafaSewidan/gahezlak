<?php

use Illuminate\Database\Seeder;

class settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Setting::create([
            'settings_category_id' => 1,
            'key' => 'store_search_area',
            'value' => 10,
            'display_name' => ' نطاق تحديد المحلات',
            'data_type' => 'number',
            'level' => 1,
        ]);

        \App\Models\Setting::create([
            'settings_category_id' => 2,
            'key' => 'points_factor',
            'value' => 0.5,
            'display_name' => ' قيمة النقطة بالريال',
            'data_type' => 'number',
            'level' => 2,
        ]);

        \App\Models\Setting::create([
            'settings_category_id' => 2,
            'key' => 'points_for_every_rs',
            'value' => 0.5,
            'display_name' => ' عدد النقاط علي كل ريال ',
            'data_type' => 'number',
            'level' => 1,
        ]);

        \App\Models\Setting::create([
            'settings_category_id' => 3,
            'key' => 'terms_and_conditions',
            'show_in_api' => 0,
            'value' => '<h1 style="color: #5d71ac; font-family: "Almarai", sans-serif; font-size: 14px;"><b>لوريم ايبسوم&nbsp;دولار سيت أميت ,كونسيكتيتور أدايبا<br></b><b> يسكينج أليايت,سيت دو أيوسمود تيمبور</b></h1><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;"><b><br></b></p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد</p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">أكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس</p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">أيوتي أريري دولار </p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;"><br></p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت نيولا باراياتيور.</p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;"> أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا</p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;"><br></p><p dir="rtl" style="color: #5d71ac; font-family: "Almarai", sans-serif; font-size: 14px;"><b>كيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم."</b></p><p dir="rtl" style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;"><br></p><ul><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">دولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">كياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">فوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">دولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">ايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">كيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">ألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">يلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">كونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">فيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">فوليوبتاس نيولا باراياتيور؟"</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">" أت فيرو ايوس ايت أكيوساميوس ايت أيوستو أودايو دايجنايسسايموس ديوكايميوس كيواي</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">بلاندايتاييس برايسينتايوم فوليوبتاتيوم ديلينايتاي أتكيوي كورريوبتاي كيوأوس دولوريس أيت</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">سيما يليكيوسيونت ان كيولبا كيواي أوفايكيا ديسيريونت موللايتايا انايماي, أيدي ايست لابوريوم</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">دايستا ينستايو. نام لايبيرو تيمبور, كيوم سوليوتا نوبايس ايست ايلاجينداي أوبتايو كيومكيوي</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">نايهايل ايمبيدايت كيو ماينيوس ايدي كيوود ماكسهيمي بلاسايت فاسيري بوسسايميوس,أومنايس</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">فوليوبتاس ايت ايوت أسسيو ميندايست, أومنيوس دولار ريبيللينديوس. تيمبورايبيوس أيوتيم</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">كيواس موليستاياس أكسكيبتيوراي ساينت أوككايكاتاي كيبايدايتات نون بروفايدنت</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">أيت دولوريوم فيوجا.ايت هاريوم كيوايديم ريريوم فاكايلايسايست ايت أكسبيدايتا</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">كيوايبيوسدام ايت أوت</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">أوففايكايس ديبايتايس أيوت ريريوم نيسيسسايتاتايبيوس سايبي ايفينايت يوت ايت فوليبتاتيس&nbsp;</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">ريبيودايايانداي ساينت ايت موليسفاياي نون ريكيوسانداي.اتاكيوي ايريوم ريريوم هايس تينيتور</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">أ ساباينتي ديليكتيوس, يوت أيوت رياسايندايس فوليوبتاتايبص مايوريس ألايس</li><li style="color: rgb(66, 66, 66); font-family: "Almarai", sans-serif; font-size: 14px;">كونسيكيواتور أيوت بيرفيريندايس دولورايبيوس أسبيرايوريس ريبيللات .</li></ul>',
            'display_name' => ' الشروط والأحكام',
            'data_type' => 'text',
            'level' => 1,
        ]);

    }
}
