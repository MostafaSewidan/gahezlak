<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('clients', function(Blueprint $table) {
			$table->increments('id');
			$table->string('user_name');
			$table->string('name')->nullable();
			$table->string('email')->nullable();
			$table->string('phone');
			$table->enum('gender', array('male', 'female'))->nullable();
			$table->date('d_o_b')->nullable();
			$table->string('pin_code')->nullable();
			$table->datetime('pin_code_date_expired')->nullable();
			$table->enum('status',['not_confirmed', 'active', 'deactivate'])->default('not_confirmed');
			$table->tinyInteger('accept_privacy')->default('0');
			$table->tinyInteger('mute_notification')->default('0');
			$table->datetime('last_login')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('clients');
	}
}