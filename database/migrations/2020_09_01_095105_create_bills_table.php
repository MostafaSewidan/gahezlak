<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillsTable extends Migration {

	public function up()
	{
		Schema::create('bills', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id');
			$table->text('description');
			$table->float('price');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('bills');
	}
}