<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientReviewsTable extends Migration {

	public function up()
	{
		Schema::create('client_reviews', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id');
			$table->integer('delivery_id');
			$table->integer('rate');
			$table->text('comment')->nullable();
			$table->timestamps();
			$table->integer('order_id');
		});
	}

	public function down()
	{
		Schema::drop('client_reviews');
	}
}