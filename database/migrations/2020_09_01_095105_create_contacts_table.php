<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id')->nullable();
			$table->enum('type', array('complaint', 'delivery_not_respond'));
			$table->text('contact');
			$table->tinyInteger('is_read')->default('0');
			$table->string('contactable_type');
			$table->integer('contactable_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contacts');
	}
}