<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditCardsTable extends Migration {

	public function up()
	{
		Schema::create('credit_cards', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('type', array(''));
			$table->string('name');
			$table->string('number');
			$table->string('cvv_code');
			$table->string('expired_year');
			$table->string('expired_month');
			$table->string('cardable_type');
			$table->integer('cardable_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('credit_cards');
	}
}