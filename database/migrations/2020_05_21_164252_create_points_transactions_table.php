<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePointsTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('points_transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('delivery_id');
			$table->integer('order_id')->nullable();
			$table->integer('points_before');
			$table->integer('quantity');
			$table->integer('points_after');
			$table->float('factor')->nullable();
			$table->float('balance')->nullable();
			$table->enum('type', array('order', 'change','reset'));
			$table->dateTime('expired_date')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('points_transactions');
	}
}