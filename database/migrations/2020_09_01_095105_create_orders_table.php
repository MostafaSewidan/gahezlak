<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('service_id')->nullable();
			$table->integer('sub_service_id')->nullable();
			$table->enum('type', array('service', 'store'));
			$table->integer('coupon_id')->nullable();
			$table->integer('store_id')->nullable();
			$table->integer('client_id');
			$table->integer('start_city_id')->nullable();
			$table->integer('end_city_id')->nullable();
			$table->string('client_latitude')->nullable();
			$table->string('client_longitude')->nullable();
			$table->string('target_latitude')->nullable();
			$table->string('target_longitude')->nullable();
			$table->string('handed_name')->nullable();
			$table->string('phone')->nullable();
			$table->enum('status', array('pending', 'accepted', 'delivery_load', 'arrived_to_client', 'delivery_handed', 'client_handed', 'rejected', 'canceled'))->default('pending');
			$table->enum('delivery_term', [1,2,3,4,5,6,7])->nullable();
			$table->enum('days_number' , [0,3,7,14])->nullable();
			$table->enum('payment_method', \App\Models\Order::$paymentMethods);
			$table->longText('description')->nullable();
			$table->tinyInteger('for_other_one')->default('0');
			$table->float('delivery_price')->nullable();
			$table->float('bill_price')->nullable();
			$table->float('coupon_cut')->nullable();
			$table->enum('coupon_type',['fixed','percent'])->nullable();
			$table->float('net')->nullable();
			$table->enum('commission_status', array('pending', 'paid'))->default('pending');
			$table->float('commission')->nullable();
			$table->float('commission_present')->nullable();
			$table->timestamps();
			$table->datetime('delivery_cancel_expired_date')->nullable();
			$table->string('natunalID')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('orders');
	}
}