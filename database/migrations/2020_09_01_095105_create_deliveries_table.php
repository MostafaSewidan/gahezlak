<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveriesTable extends Migration {

	public function up()
	{
		Schema::create('deliveries', function(Blueprint $table) {
			$table->increments('id');
//			$table->integer('region_id');
			$table->string('user_name');
			$table->string('name');
			$table->string('phone');
			$table->string('email');
			$table->enum('status', array('pending', 'active', 'not_active', 'rejected'))->default('pending');
			$table->string('password')->nullable();
			$table->string('pin_code')->nullable();
			$table->datetime('pin_code_date_expired')->nullable();
			$table->datetime('last_login')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->tinyInteger('is_set_location')->default(0);
			$table->tinyInteger('accept_privacy')->default('0');
			$table->tinyInteger('mute_notification')->default('0');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('deliveries');
	}
}