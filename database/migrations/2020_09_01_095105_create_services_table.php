<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration {

	public function up()
	{
		Schema::create('services', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('title');
			$table->tinyInteger('order')->nullable();
			$table->integer('parent_id')->nullable();
			$table->float('search_area')->default(0);
			$table->string('type');
		});
	}

	public function down()
	{
		Schema::drop('services');
	}
}