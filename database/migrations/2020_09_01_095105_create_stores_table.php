<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration {

	public function up()
	{
		Schema::create('stores', function(Blueprint $table) {
			$table->increments('id');
			$table->string('phone')->nullable();
			$table->string('name');
			$table->tinyInteger('order')->nullable();
			$table->tinyInteger('proposed')->default('0');
			$table->tinyInteger('is_active')->default('1');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('stores');
	}
}