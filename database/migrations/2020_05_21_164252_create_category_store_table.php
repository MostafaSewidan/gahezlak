<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryStoreTable extends Migration {

	public function up()
	{
		Schema::create('category_store', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('store_id');
			$table->integer('category_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('category_store');
	}
}