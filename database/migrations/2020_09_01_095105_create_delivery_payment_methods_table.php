<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryPaymentMethodsTable extends Migration {

	public function up()
	{
		Schema::create('delivery_payment_methods', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('delivery_id');
			$table->enum('payment_method' , \App\Models\Order::$paymentMethods);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('delivery_payment_methods');
	}
}