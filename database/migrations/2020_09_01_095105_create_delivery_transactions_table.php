<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('delivery_transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('delivery_id');
		});
	}

	public function down()
	{
		Schema::drop('delivery_transactions');
	}
}