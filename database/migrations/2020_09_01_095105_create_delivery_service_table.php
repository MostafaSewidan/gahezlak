<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryServiceTable extends Migration {

	public function up()
	{
		Schema::create('delivery_service', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('service_id');
			$table->integer('delivery_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('delivery_service');
	}
}