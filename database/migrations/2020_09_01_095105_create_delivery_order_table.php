<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryOrderTable extends Migration {

	public function up()
	{
		Schema::create('delivery_order', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id');
			$table->integer('delivery_id');
			$table->float('price')->nullable();
			$table->enum('status', array('pending', 'offer' , 'accepted', 'rejected', 'canceled'));
			$table->float('deliver_expected_time')->nullable();
			$table->float('minuets_far_target_location')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('delivery_order');
	}
}