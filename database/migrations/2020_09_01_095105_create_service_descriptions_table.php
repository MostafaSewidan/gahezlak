<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceDescriptionsTable extends Migration {

	public function up()
	{
		Schema::create('service_descriptions', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('service_id');
			$table->text('description');
		});
	}

	public function down()
	{
		Schema::drop('service_descriptions');
	}
}