<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreAddressesTable extends Migration {

	public function up()
	{
		Schema::create('store_addresses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('store_id');
			$table->string('title')->nullable();
			$table->enum('type', array('main', 'normal'));
			$table->string('latitude');
			$table->string('longitude');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('store_addresses');
	}
}