<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCouponsTable extends Migration {

	public function up()
	{
		Schema::create('coupons', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code');
			$table->integer('use_frequency');
			$table->datetime('expired_date');
			$table->datetime('start_date');
			$table->float('amount');
			$table->enum('type', array('fixed', 'percent'));
			$table->tinyInteger('is_active')->default('1');
			$table->float('max_discount')->default('0.00');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('coupons');
	}
}