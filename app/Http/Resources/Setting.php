<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \App\Models\Setting as SettingModel;

class Setting extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        $allSettings = \App\Models\Setting::where('show_in_api' , 1)->get();
        $settingArray = [];
        foreach ($allSettings as $set) {
            if ($set->data_type == 'fileWithPreview' && $set->photo) {
                $settingArray +=
                    [
                        $set->key => asset($set->photo->path)
                    ];

            } else {
                $settingArray +=
                    [
                        $set->key => $set->value
                    ];
            }

        }
        return $settingArray;
    }
}
