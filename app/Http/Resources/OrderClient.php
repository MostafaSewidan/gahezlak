<?php

namespace App\Http\Resources;

use App\Models\Setting;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderClient extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'gender' => $this->gender,
            'photo' => $this->photo ? asset($this->photo->path) : null,
            'rate' => $this->clientReviews()->count() ? number_format($this->clientReviews()->avg('rate'),1) : '0.0',
        ];
    }
}
