<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Delivery extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'personal_photo' => $this->personal_photo,
            'front_car_photo' => $this->front_car_photo,
            'back_car_photo' => $this->back_car_photo,
            'delivery_rate' => $this->serviceProviderReviews()->count() ? number_format($this->serviceProviderReviews()->avg('rate'), 1) : '0.0',
        ];
    }
}
