<?php

namespace App\Http\Resources;

use App\Models\Setting;
use App\MyHelper\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class Store extends JsonResource
{
    protected $points;
    public function points($points = 0){
        $this->points = $points;
        return $this;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        $area = Helper::settingValue('store_search_area' , 0);
        $distance = $this->addresses()->count() && $this->points != 0 ? $this->addresses()->select(\DB::raw('*, ( 6367 * acos( cos( radians(' . $this->points['latitude'] . ') ) * cos( radians( latitude ) ) * 
                    cos( radians( longitude ) - radians(' . $this->points['longitude'] . ') ) + sin( radians(' . $this->points['latitude'] . ') ) * sin( radians( latitude ) ) ) ) AS distance'))->having('distance', '<', $area)->first() : null;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'distance' => $distance != null ? number_format($distance->distance,1) .'km' : '0.0km',
            "address" => $distance ? new Address($distance): new Address($this->addresses()->first()),
            "rate" => $this->reviews()->count() ? number_format($this->reviews()->avg('rate'),1) : '0.0',
//            TODO 'photo' => $this->photo ? asset($this->photo->path) : null,
            'photo' => asset('photos/market.jpg'),
        ];
    }

    public static function collection($resource){
        return new StoreCollection($resource);
    }
}
