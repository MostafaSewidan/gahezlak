<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Bill extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'order_id'       => $this->order_id,
            'description'       => $this->description,
            'price'       => $this->price,
        ];
    }
}
