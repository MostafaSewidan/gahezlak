<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Service extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'descriptions' => ServiceDescriptions::collection($this->descriptions()->get()),
            'sub_service' => Service::collection($this->children()->get()),
            'photo' => $this->image
        ];
    }
}
