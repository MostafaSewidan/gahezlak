<?php

namespace App\Http\Resources;

use App\Models\Setting;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_name' => $this->user_name,
            'email' => $this->email,
            'name' => $this->name,
            'phone' => $this->phone,
            'gender' => $this->gender,
            'd_o_b' => Carbon::parse($this->d_o_b)->format('Y-m-d'),
            'day' => Carbon::parse($this->d_o_b)->format('d'),
            'month' => Carbon::parse($this->d_o_b)->format('m'),
            'year' => Carbon::parse($this->d_o_b)->format('Y'),
            'accept_privacy' => $this->accept_privacy,
            'mute_notification' => $this->mute_notification,
            'photo' => $this->photo ? asset($this->photo->path) : null,
            'rate' => $this->clientReviews()->count() ? number_format($this->clientReviews()->avg('rate'),1) : '0.0',
        ];
    }
}
