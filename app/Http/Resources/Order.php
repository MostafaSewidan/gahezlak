<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    protected $responseTo = 'client';
    protected $deliveryLocation = null;
    public function responseTo($responseTo = 'client' , $deliveryLocation = null){
        $this->responseTo = $responseTo;
        $this->deliveryLocation = $deliveryLocation;
        return $this;
    }
    /**
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $returnObject = [
            'id' => $this->id,
            'status' => $this->status,
            'type' => $this->type,

            'for_other_one' => $this->for_other_one,
            'handed_name' => $this->handed_name,
            'handed_phone' => $this->phone,
            'delivery_term' => $this->delivery_term,
            'days_number' => $this->days_number,
            'payment_method' => $this->payment_method,
            'delivery_price' => $this->delivery_price,
            'bill_price' => $this->bill_price,
            'net' => $this->net,
            'coupon_cut' => $this->coupon_cut,
            'coupon_type' => $this->coupon_type,
            'natunalID' => $this->natunalID,
            'description' => $this->description,
            'description_photo' => $this->description_photo,
            'bill_photo' => $this->bill_photo,

            'client_location' => [
                'latitude' => $this->client_latitude,
                'longitude' =>$this->client_longitude ,
            ],
            'target_location' => [
                'latitude' => $this->target_latitude,
                'longitude' => $this->target_longitude,
            ],
            'bill' => Bill::collection($this->bill),
            'service' => new Service($this->service),
            'sub_service' => new Service($this->subService),
            'store' => new Store($this->store),
            'start_city' => new City($this->startCity),
            'end_city' => new City($this->endCity),
            'delivery_offer' => new DeliveryOffer($this->deliveries()->where('delivery_order.status','accepted')->first()),
        ];

        if($this->responseTo == 'delivery')
        {
            if($this->status == 'pending')
            {
                $delivery = auth($this->responseTo == 'client' ? 'api_client' : 'api_delivery')->user();

                $returnObject +=[
                    'distances' => \App\Models\Order::distances([
                        'client_latitude' => $this->client_latitude,
                        'client_longitude' => $this->client_longitude,
                        'target_latitude' => $this->target_latitude,
                        'target_longitude' => $this->target_longitude,
                        'delivery_latitude' => $delivery->latitude,
                        'delivery_longitude' => $delivery->longitude,
                    ])
                ];

            }else{

                $returnObject +=[
                    'distances' => []
                ];

            }

            $returnObject += [
                'ordered_after' =>  $this->ordered_after,
                'delivery_cancel_expired_date' => $this->delivery_cancel_expired_date ?Carbon::parse($this->delivery_cancel_expired_date)->toDateTimeString() : null,
                'commission_status' =>  $this->commission_status,
                'commission' =>  $this->commission_status,
                'commission_present' =>  $this->commission_status,
                'client' => new OrderClient($this->client),
            ];

        }elseif ($this->responseTo == 'client')
        {
            $returnObject += [
                'status_text' =>  $this->client_status_text,
//                'delivery' => new Delivery($this->delivery),
            ];
        }



        return $returnObject;
    }

    public static function collection($resource){
        return new OrderCollection($resource);
    }
}
