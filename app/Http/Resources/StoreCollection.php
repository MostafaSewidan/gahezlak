<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StoreCollection extends ResourceCollection
{
    protected $points;

    public function points($points = 0){
        $this->points = $points;
        return $this;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request){
        return $this->collection->map(function(Store $resource) use($request){
            return $resource->points($this->points)->toArray($request);
        })->all();
    }

}
