<?php

namespace App\Http\Resources\Provider;

use App\Models\Setting;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Provider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'user_name' => $this->user_name,
            'phone' => $this->phone,
            'accept_privacy' => $this->accept_privacy,
            'mute_notification' => $this->mute_notification,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'id_card_photo' => $this->id_card_photo,
            'personal_photo' => $this->personal_photo,
            'driving_license_photo' => $this->driving_license_photo,
            'form_photo' => $this->form_photo,
            'front_car_photo' => $this->front_car_photo,
            'back_car_photo' => $this->back_car_photo,
            'criminal_record_photo' => $this->criminal_record_photo,
            'delivery_rate' => $this->serviceProviderReviews()->count() ? number_format($this->serviceProviderReviews()->avg('rate'), 1) : '0.0',
            'points' => [
                'last_points' => $this->last_points,
                'last_points_in_rs' => $this->last_points_in_rs,
                'last_points_expired_date' => $this->last_points_expired_date,
                'last_points_expired_date_no_format' => $this->last_points_expired_date_no_format,
            ],
        ];
    }
}
