<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryOffer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->pivot->order_id,
            'price' => $this->pivot->price,
            'status' => $this->pivot->status,
            'deliver_expected_time' => (int)$this->pivot->deliver_expected_time,
            'minuets_far_target_location' => (int)$this->pivot->minuets_far_target_location,
            'order' => new Order($this),
            'delivery' => new Delivery($this->deliveries()->find($this->pivot->delivery_id)),
        ];
    }
}
