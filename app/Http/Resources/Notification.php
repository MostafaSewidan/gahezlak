<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    protected $responseTo = 'client';
    public function responseTo($responseTo = 'client'){
        $this->responseTo = $responseTo;
        return $this;
    }
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $resource = $this->resources;

        $resource = $this->type == 'order' ? (new Order($this->notifiable))->responseTo($this->responseTo) : new $resource($this->notifiable);

        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'is_read' => $this->pivot->is_read,
            'type' => $this->type,
            'data' => $resource,
        ];
    }

    public static function collection($resource){
        return new NotificationCollection($resource);
    }
}
