<?php

namespace App\Http\Resources;

use App\Models\OrderProduct;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
        ];
    }
}
