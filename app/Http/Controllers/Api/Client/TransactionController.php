<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\Address;
use App\Http\Resources\PointsReport;
use App\Models\Client;
use App\Models\Setting;
use App\MyHelper\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends ParentApi
{
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_client';
        $this->model = new Client();
        $this->apiResource = '\App\Http\Resources\Client';
        $this->table = 'clients';
        $this->uniqueRow = 'phone';
        $this->sendPinCodeErrorMessage = 'رقم الهاتف غير صحيح';
    }

}
