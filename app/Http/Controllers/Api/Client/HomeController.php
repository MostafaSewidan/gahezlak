<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\Category as ResourcesCategory;
use App\Http\Resources\Service as ResourcesService;
use App\Http\Resources\Ads as ResourcesAd;
use App\Http\Resources\Country as ResourcesCountry;
use App\Http\Resources\StoreCollection;
use App\Models\Advertisement;
use App\Models\Category;
use App\Models\City;
use App\Models\Client;
use App\Models\Country;
use App\Models\Order;
use App\Models\Region;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Store;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends ParentApi
{

    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_client';
        $this->model = new Client();
    }

    public function settings()
    {
        return $this->helper->responseJson(1, 'تمت العملية', new \App\Http\Resources\Setting(''));
    }

    public function home(Request $request)
    {
        $services = Service::where('parent_id', null)->where(function ($q) use ($request) {

        })->orderBy('order')->get();

        $ads = Advertisement::where(function ($q) use ($request) {

        })->orderBy('order')->get();

        $categories = Category::where(function ($q) use ($request) {

            if ($request->category_id) {
                $q->where('id', $request->category_id);
            }

            if ($request->key) {
                $q->where('name', 'LIKE', '%' . $request->key . '%')
                    ->orwhere('description', 'LIKE', '%' . $request->key . '%');
            }

        })->orderBy('order')->get();

        return $this->helper->responseJson(1, 'تمت العملية بنجاح',
            [
                'categories' => ResourcesCategory::collection($categories),
                'services' => ResourcesService::collection($services),
                'ads' => ResourcesAd::collection($ads),
            ]);

    }

    public function countries(Request $request)
    {
        $records = Country::where(function ($q) use ($request) {

            if ($request->country_id) {
                $q->where('id', $request->country_id);
            }

        })->get();

        return (ResourcesCountry::collection($records))->additional([
            "status" => 1,
            "massage" => "تمت العملية بنجاح",
        ]);
    }

    public function PaymentMethods(Request $request)
    {
        return $this->helper->responseJson(1, 'تمت العملية', Order::$paymentMethods);
    }

    public function stores(Request $request)
    {
        $rules =
            [
                'latitude' => 'required',
                'longitude' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $records = Store::activation()->CheckArea($request->only('latitude', 'longitude'))->where(function ($q) use ($request) {

            if ($request->key) {

                $q->where(function ($q) use ($request){

                    $q->where('name', 'LIKE', '%' . $request->key . '%')
                        ->orWhereHas('categories', function ($q) use ($request) {

                            $q->where('name', 'LIKE', '%' . $request->key . '%')
                                ->orwhere('description', 'LIKE', '%' . $request->key . '%');

                        })
                        ->orWhere('phone', 'LIKE', '%' . $request->key . '%');
                });

            }

            if ($request->store_id) {

                $q->where('id', $request->store_id);

            }

            if ($request->category_id) {

                $q->whereHas('categories', function ($q) use ($request) {

                    $q->where('categories.id', $request->category_id);

                });
            }

        })->orderBy('order')->paginate(10);

        return (new StoreCollection($records))->points($request->only('latitude','longitude'))->additional([
            "status" => 1,
            "massage" => "تمت العملية",
        ]);
    }

    public function proposedStores(Request $request)
    {
        $rules =
            [
                'latitude' => 'required',
                'longitude' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $records = Store::activation()->proposed()->CheckArea($request->only('latitude', 'longitude'))->orderBy('order')->paginate(10);

        return (new StoreCollection($records))->points($request->only('latitude','longitude'))->additional([
            "status" => 1,
            "massage" => "تمت العملية",
        ]);
    }
}
