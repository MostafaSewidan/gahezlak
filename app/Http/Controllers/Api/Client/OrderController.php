<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\NotificationOrder;
use App\Http\Resources\Order as ResourceOrder;
use App\Http\Resources\Coupon as ResourceCoupon;
use App\Models\Coupon;
use App\Models\Delivery;
use App\Models\Order;
use App\Models\Service;
use App\Models\Store;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends ParentApi
{

    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_client';
        $this->model = new Order();
    }

    /**
     * @param mixed $type
     * @param mixed $col
     * @return mixed
     */
    public function getService($type, $col = 'id')
    {
        return Service::where('type', $type)->first() ? Service::where('type', $type)->first()->$col : null;
    }

    public function useCoupon($code, $order)
    {
        $coupon = Coupon::where(['code' => $code, 'is_active' => 1])->where('use_frequency', '>', 0)
            ->whereDate('expired_date', '>=', Carbon::now()->toDateTimeString())
            ->whereDate('start_date', '<=', Carbon::now()->toDateTimeString())->first();

        if ($coupon) {

            $coupon->decrement('use_frequency', 1);

            if ($coupon->use_frequency == 0) {
                $coupon->is_active = 0;
                $coupon->save();
            }

            $couponAmount = $coupon->amount;
            $couponAmount = $coupon->max_discount < $couponAmount ? $coupon->max_discount : $couponAmount;

            $order->coupon_cut = $couponAmount;
            $order->coupon_type = $coupon->type;
            $order->coupon_id = $coupon->id;
            $order->save();

            return $coupon;
        }

        return null;
    }

    public function checkCoupon(Request $request)
    {
        $user = $request->user('api_client');

        $rules =
            [
                'coupon_code' => 'required|exists:coupons,code',
            ];
        $messages = [
            'coupon_code.required' => 'ادخل كود الكوبون',
            'coupon_code.exists' => 'كود الكوبون غير صحيح',
        ];

        $data = validator()->make($request->all(), $rules, $messages);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $coupon = Coupon::where(['code' => $request->coupon_code, 'is_active' => 1])->where('use_frequency', '>', 0)
            ->whereDate('expired_date', '>=', Carbon::now()->toDateTimeString())
            ->whereDate('start_date', '<=', Carbon::now()->toDateTimeString())->first();

        if ($coupon)
            return $this->helper->responseJson(1, 'تم إضافة كوبون الخصم', new ResourceCoupon($coupon));

        return $this->helper->responseJson(0, 'الكوبون غير صالح');
    }

    public function listOrders(Request $request)
    {
        $user = $request->user('api_client');

        $orders = $user->orders()->where(function ($q) use ($request) {

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }

            if ($request->order_id) {
                $q->where('status', $request->status);
            }

        })->latest()->paginate(15);

        return (ResourceOrder::collection($orders))->additional([
            'status' => 1,
            'message' => 'تمت العملية',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'store_id' => 'required|exists:stores,id',
                'client_latitude' => 'required',
                'client_longitude' => 'required',
                'target_latitude' => 'required',
                'target_longitude' => 'required',
                'delivery_term' => 'required',
                'for_other_one' => 'required|in:0,1',
                'payment_method' => 'required|in:' . implode(',', Order::$paymentMethods),
                'coupon_code' => 'nullable|exists:coupons,code',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->for_other_one == 1 && $request->paymenCheckTypet_method == 'cache')
            return $this->helper->responseJson(0, 'الطلب لشخص أخر لا يقبل الدفع عند الإستلام');

        if (!$request->hasFile('photo') && !$request->input('description'))
            return $this->helper->responseJson(0, 'الرجاء كتابة الطلب أو إدراج صورة');

        $distance = $this->helper->distance($request->client_latitude, $request->client_longitude, $request->target_latitude, $request->target_longitude);

        if ($distance > $this->helper->settingValue('store_search_area', 0))
            return $this->helper->responseJson(0, 'هذا الطلب خارج نطاق مكان تسليم الطلب');

        DB::beginTransaction();

        $order = $user->orders()->create([
            'type' => 'store',
            'store_id' => $request->store_id,
            'client_latitude' => $request->client_latitude,
            'client_longitude' => $request->client_longitude,
            'target_latitude' => $request->target_latitude,
            'target_longitude' => $request->target_longitude,
            'delivery_term' => $request->delivery_term,
            'for_other_one' => $request->for_other_one,
            'payment_method' => $request->payment_method,
            'description' => $request->input('description'),
        ]);

        if ($request->input('coupon_code')) {
            $useCoupon = $this->useCoupon($request->coupon_code, $order);

            if (!$useCoupon)
                return $this->helper->responseJson(0, 'كود الكوبون غير صالح');
        }

        if ($request->hasFile('photo'))
            Photo::addPhoto($request->file('photo'), $order, 'orders', ['size' => 600, 'relation' => 'photos']);

        $deliveryIds = Delivery::CheckArea(
            ['latitude' => $request->target_latitude, 'longitude' => $request->target_longitude],
            ['distance' => Helper::settingValue('store_search_area', 0), 'payment_method' => $request->payment_method]
        );

        if (count($deliveryIds)) {
            $order->deliveries()->attach($deliveryIds);

            $this->helper->sendNotification($order, $deliveryIds, 'deliveries', 'يوجد لديك طلب جديد ', 'قام عميل بالطلب من المحل في نطاق تواجدك , العميل الأن في انتظار الرد', 'order', new NotificationOrder($order));
        } else {

            DB::rollBack();

            return $this->helper->responseJson(0, 'لا يوجد مناديب في نطاق منطقتك');
        }

        DB::commit();

        $order->refresh();
        return $this->helper->responseJson(1, 'تم التقدم بالطلب بنجاح , برجاء انتظار عروض مناديبنا', new ResourceOrder($order));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function personalShopperOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'store_id' => 'nullable|exists:stores,id',
                'client_latitude' => 'required',
                'client_longitude' => 'required',
                'target_latitude' => 'required',
                'target_longitude' => 'required',
                'for_other_one' => 'required|in:0,1',
                'payment_method' => 'required|in:' . implode(',', Order::$paymentMethods),
                'coupon_code' => 'nullable|exists:coupons,code',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->for_other_one == 1 && $request->payment_method == 'cache')
            return $this->helper->responseJson(0, 'الطلب لشخص أخر لا يقبل الدفع عند الإستلام');

        if (!$request->hasFile('photo') && !$request->input('description'))
            return $this->helper->responseJson(0, 'الرجاء كتابة الطلب أو إدراج صورة');

        $distance = $this->helper->distance($request->client_latitude, $request->client_longitude, $request->target_latitude, $request->target_longitude);

        if ($distance > $this->getService('personal_shopper', 'search_area'))
            return $this->helper->responseJson(0, 'هذا الطلب خارج نطاق مكان تسليم الطلب');

        DB::beginTransaction();


        $order = $user->orders()->create([
            'service_id' => $this->getService('personal_shopper', 'id'),
            'type' => 'service',
            'store_id' => $request->input('store_id'),
            'client_latitude' => $request->client_latitude,
            'client_longitude' => $request->client_longitude,
            'target_latitude' => $request->target_latitude,
            'target_longitude' => $request->target_longitude,
            'for_other_one' => $request->for_other_one,
            'payment_method' => $request->payment_method,
            'description' => $request->input('description'),
        ]);

        if ($request->input('coupon_code')) {
            $useCoupon = $this->useCoupon($request->coupon_code, $order);

            if (!$useCoupon)
                return $this->helper->responseJson(0, 'كود الكوبون غير صالح');
        }

        if ($request->hasFile('photo'))
            Photo::addPhoto($request->file('photo'), $order, 'orders', ['size' => 600, 'relation' => 'photos']);


        $deliveryIds = Delivery::CheckArea(
            ['latitude' => $request->target_latitude, 'longitude' => $request->target_longitude],
            [
                'distance' => $this->getService('personal_shopper', 'search_area'),
                'service_id' => $this->getService('personal_shopper'),
                'payment_method' => $request->payment_method,
            ]
        );

        if (count($deliveryIds)) {
            $order->deliveries()->attach($deliveryIds);
            $this->helper->sendNotification($order, $deliveryIds, 'deliveries', 'يوجد لديك طلب جديد ', ' , العميل الأن في انتظار الرد ' . ' ( ' . $this->getService('send_gifts', 'title') . ' ) ' . 'قام عميل بعمل طلب من خدمة ', 'order', new NotificationOrder($order));
        } else {

            DB::rollBack();

            return $this->helper->responseJson(0, 'لا يوجد مناديب في نطاق منطقتك');
        }

        DB::commit();

        $order->refresh();
        return $this->helper->responseJson(1, 'تم التقدم بالطلب بنجاح , برجاء انتظار عروض مناديبنا', new ResourceOrder($order));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function askService(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'sub_service_id' => 'nullable|exists:services,id',
                'client_latitude' => 'required',
                'client_longitude' => 'required',
                'payment_method' => 'required|in:' . implode(',', Order::$paymentMethods),
                'coupon_code' => 'nullable|exists:coupons,code',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->for_other_one == 1 && $request->payment_method == 'cache')
            return $this->helper->responseJson(0, 'الطلب لشخص أخر لا يقبل الدفع عند الإستلام');

        if (!$request->hasFile('photo') && !$request->input('description'))
            return $this->helper->responseJson(0, 'الرجاء كتابة الطلب أو إدراج صورة');


        DB::beginTransaction();

        $order = $user->orders()->create([
            'service_id' => $this->getService('ask_service'),
            'type' => 'service',
            'sub_service_id' => $request->input('sub_service_id'),
            'client_latitude' => $request->client_latitude,
            'client_longitude' => $request->client_longitude,
            'payment_method' => $request->payment_method,
            'description' => $request->input('description'),
        ]);

        if ($request->input('coupon_code')) {
            $useCoupon = $this->useCoupon($request->coupon_code, $order);

            if (!$useCoupon)
                return $this->helper->responseJson(0, 'كود الكوبون غير صالح');
        }

        if ($request->hasFile('photo'))
            Photo::addPhoto($request->file('photo'), $order, 'orders', ['size' => 600, 'relation' => 'photos']);

        $deliveryIds = Delivery::CheckArea(

            ['latitude' => $request->client_latitude, 'longitude' => $request->client_longitude],
            [
                'distance' => $this->getService('ask_service', 'search_area'),
                'service_id' => $this->getService('ask_service'),
                'payment_method' => $request->payment_method,
            ]
        );

        if (count($deliveryIds)) {
            $order->deliveries()->attach($deliveryIds);
            $this->helper->sendNotification($order, $deliveryIds, 'deliveries', 'يوجد لديك طلب جديد ', ' , العميل الأن في انتظار الرد ' . ' ( ' . $this->getService('send_gifts', 'title') . ' ) ' . 'قام عميل بعمل طلب من خدمة ', 'order', new NotificationOrder($order));
        } else {

            DB::rollBack();

            return $this->helper->responseJson(0, 'لا يوجد مناديب في نطاق منطقتك');
        }

        DB::commit();

        $order->refresh();
        return $this->helper->responseJson(1, 'تم التقدم بالطلب بنجاح , برجاء انتظار عروض مناديبنا', new ResourceOrder($order));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendGifts(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'payment_method' => 'required|in:' . implode(',', Order::$paymentMethods),
                'coupon_code' => 'nullable|exists:coupons,code',
                'description' => 'required',
                'handed_name' => 'required',
                'handed_phone' => 'required|regex:/(05)[0-9]{8}/',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->for_other_one == 1 && $request->payment_method == 'cache')
            return $this->helper->responseJson(0, 'الطلب لشخص أخر لا يقبل الدفع عند الإستلام');

        if (!$request->hasFile('photo') && !$request->input('description'))
            return $this->helper->responseJson(0, 'الرجاء كتابة الطلب أو إدراج صورة');


        DB::beginTransaction();

        $order = $user->orders()->create([
            'service_id' => $this->getService('send_gifts'),
            'type' => 'service',
            'payment_method' => $request->payment_method,
            'handed_name' => $request->handed_name,
            'phone' => $request->handed_phone,
            'description' => $request->description,
        ]);

        if ($request->input('coupon_code')) {
            $useCoupon = $this->useCoupon($request->coupon_code, $order);

            if (!$useCoupon)
                return $this->helper->responseJson(0, 'كود الكوبون غير صالح');
        }

        if ($request->hasFile('photo'))
            Photo::addPhoto($request->file('photo'), $order, 'orders', ['size' => 600, 'relation' => 'photos']);


        $deliveryIds = Delivery::CheckArea(

            ['latitude' => $request->client_latitude, 'longitude' => $request->client_longitude],
            [
                'distance' => $this->getService('send_gifts', 'search_area'),
                'service_id' => $this->getService('send_gifts'),
                'payment_method' => $request->payment_method,
            ]
        );

        if (count($deliveryIds)) {
            $order->deliveries()->attach($deliveryIds);
            $this->helper->sendNotification($order, $deliveryIds, 'deliveries', 'يوجد لديك طلب جديد ', ' ( ' . $this->getService('send_gifts', 'title') . ' ) ' . 'قام عميل بعمل طلب من خدمة ', 'order', new NotificationOrder($order));
        }
        else{

            DB::rollBack();

            return $this->helper->responseJson(0, 'لا يوجد مناديب في نطاق منطقتك');
        }

        DB::commit();

        $order->refresh();
        return $this->helper->responseJson(1, 'تم التقدم بالطلب بنجاح', new ResourceOrder($order));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendShipmentsInMyArea(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'client_latitude' => 'required',
                'client_longitude' => 'required',
                'target_latitude' => 'required',
                'target_longitude' => 'required',
                'days_number' => 'required|in:0,3,7,14',
                'payment_method' => 'required|in:' . implode(',', Order::$paymentMethods),
                'coupon_code' => 'nullable|exists:coupons,code',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->for_other_one == 1 && $request->payment_method == 'cache')
            return $this->helper->responseJson(0, 'الطلب لشخص أخر لا يقبل الدفع عند الإستلام');

        if (!$request->hasFile('photo') && !$request->input('description'))
            return $this->helper->responseJson(0, 'الرجاء كتابة الطلب أو إدراج صورة');

        $distance = $this->helper->distance($request->client_latitude, $request->client_longitude, $request->target_latitude, $request->target_longitude);

        if ($distance > $this->getService('in_my_area', 'search_area'))
            return $this->helper->responseJson(0, 'هذا الطلب خارج نطاق مكان تسليم الطلب');


        DB::beginTransaction();

        $order = $user->orders()->create([
            'service_id' => $this->getService('send_shipments'),
            'type' => 'service',
            'sub_service_id' => $this->getService('in_my_area'),
            'client_latitude' => $request->client_latitude,
            'client_longitude' => $request->client_longitude,
            'target_latitude' => $request->target_latitude,
            'target_longitude' => $request->target_longitude,
            'payment_method' => $request->payment_method,
            'description' => $request->input('description'),
        ]);

        if ($request->input('coupon_code')) {
            $useCoupon = $this->useCoupon($request->coupon_code, $order);

            if (!$useCoupon)
                return $this->helper->responseJson(0, 'كود الكوبون غير صالح');
        }

        if ($request->hasFile('photo'))
            Photo::addPhoto($request->file('photo'), $order, 'orders', ['size' => 600, 'relation' => 'photos']);


        $deliveryIds = Delivery::CheckArea(

            ['latitude' => $request->client_latitude, 'longitude' => $request->client_longitude],
            [
                'distance' => $this->getService('in_my_area', 'search_area'),
                'service_id' => $this->getService('in_my_area'),
                'payment_method' => $request->payment_method,
            ]
        );

        if (count($deliveryIds)) {
            $order->deliveries()->attach($deliveryIds);
            $this->helper->sendNotification($order, $deliveryIds, 'deliveries', 'يوجد لديك طلب جديد ', ' , العميل الأن في انتظار الرد ' . ' ( ' . $this->getService('in_my_area', 'title') . ' ) ' . 'قام عميل بعمل طلب من خدمة  أرسال اغارض  ', 'order', new NotificationOrder($order));
        } else {

            DB::rollBack();

            return $this->helper->responseJson(0, 'لا يوجد مناديب في نطاق منطقتك');
        }

        DB::commit();

        $order->refresh();
        return $this->helper->responseJson(1, 'تم التقدم بالطلب بنجاح , برجاء انتظار عروض مناديبنا', new ResourceOrder($order));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendShipmentWithTravelers(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'start_city_id' => 'required|exists:cities,id',
                'end_city_id' => 'required|exists:cities,id',
                'days_number' => 'required|in:0,3,7,14',
                'payment_method' => 'required|in:' . implode(',', Order::$paymentMethods),
                'coupon_code' => 'nullable|exists:coupons,code',
                'photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'description' => 'required',
                'natunalID' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->start_city_id == $request->end_city_id)
            return $this->helper->responseJson(0, 'أنت تطلب نقل أغراض في نفس المدينة يمكنك استخدام خدمة نقل الأغراض في نطاق منطقتك');

        if ($request->for_other_one == 1 && $request->payment_method == 'cache')
            return $this->helper->responseJson(0, 'الطلب لشخص أخر لا يقبل الدفع عند الإستلام');


        DB::beginTransaction();

        $order = $user->orders()->create([
            'service_id' => $this->getService('send_shipments'),
            'type' => 'service',
            'sub_service_id' => $this->getService('travelers'),
            'start_city_id' => $request->client_latitude,
            'end_city_id' => $request->client_longitude,
            'payment_method' => $request->payment_method,
            'description' => $request->description,
            'natunalID' => $request->natunalID,
        ]);

        if ($request->input('coupon_code')) {
            $useCoupon = $this->useCoupon($request->coupon_code, $order);

            if (!$useCoupon)
                return $this->helper->responseJson(0, 'كود الكوبون غير صالح');
        }

        if ($request->hasFile('photo'))
            Photo::addPhoto($request->file('photo'), $order, 'orders', ['size' => 600, 'relation' => 'photos']);

        //TODO filtering
        $deliveryIds = Delivery::activation()
            ->hasService($this->getService('travelers'));
//            ->CheckArea(['latitude' => $request->client_latitude ,'longitude' => $request->client_longitude] , $this->getService('travelers' , 'search_area'));

        if (count($deliveryIds)) {
            $order->deliveries()->attach($deliveryIds);
            $this->helper->sendNotification($order, $deliveryIds, 'deliveries', 'يوجد لديك طلب جديد ', ' , العميل الأن في انتظار الرد ' . ' ( ' . $this->getService('travelers', 'title') . ' ) ' . 'قام عميل بعمل طلب من خدمة أرسال اغارض ', 'order', new NotificationOrder($order));
        }
//        else{
//
//            DB::rollBack();
//
//            return $this->helper->responseJson(0, 'لا يوجد مناديب في نطاق منطقتك');
//        }

        DB::commit();

        $order->refresh();

        return $this->helper->responseJson(1, 'تم التقدم بالطلب بنجاح , برجاء انتظار عروض مناديبنا', new ResourceOrder($order));
    }

}
