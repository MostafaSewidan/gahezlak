<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Models\Client;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;

class AuthController extends ParentApi
{
//////////////////////////////
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_client';
        $this->model = new Client();
        $this->apiResource = '\App\Http\Resources\Client';
        $this->table = 'clients';
        $this->uniqueRow = 'phone';
        $this->sendPinCodeErrorMessage = 'رقم الهاتف غير صحيح';
    }

    //confirm account after sending pin code with expired time after 2 minutes
    public function confirmAccount(Request $request)
    {
        // if user register or login ( out auth )

        $rules =
            [
                'user_name' => 'required|exists:clients,user_name',
                'phone' => 'required|exists:clients,phone',
                'pin_code' => 'required|numeric',
                'accept_privacy' => 'required|in:1,0',
                'gender' => 'nullable|in:male,female',
                'd_o_b' => 'nullable|date',
                'email' => 'nullable|email',
                'token' => 'required',
                'serial_number' => 'required',
                'os' => 'required|in:android,ios',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }


        $client = $this->model->where(['phone' => $request->phone, 'user_name' => $request->user_name])->first();

        if ($client) {
            if ($client->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($client->pin_code_date_expired)) {

                    //update pin code and confirm user if not confirmed
                    $client->pin_code = $this->getPinCode();
                    $client->save();

                    if ($client->status == 'not_confirmed')
                        $client->status = 'active';

                    if ($request->input('gender'))
                        $client->gender = $request->gender;

                    if ($request->input('email'))
                        $client->email = $request->email;

                    if ($request->input('accept_privacy'))
                        $client->accept_privacy = $request->accept_privacy;

                    if ($request->input('d_o_b'))
                        $client->d_o_b = Carbon::parse($request->d_o_b)->toDateTimeString();

                    $client->save();

                    //create passport token
                    $token = $client->createToken('android')->accessToken;

                    if ($client->tokens()->where('serial_number', $request->serial_number)->first()) {
                        $phone_token = $client->tokens()->where('serial_number', $request->serial_number)->first();
                        $phone_token->update(
                            [
                                'token' => $request->token,
                                'phone_type' => $request->os,
                                'serial_number' => $request->serial_number
                            ]);

                    } else {

                        $client->tokens()->create(['token' => $request->token, 'phone_type' => $request->os, 'serial_number' => $request->serial_number]);
                    }

                    return $this->helper->responseJson(1, 'success', ['token' => $token, 'user' => new \App\Http\Resources\Client($client)]);
                }

                return $this->helper->responseJson(0, 'كود التاكيد غير صالح , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }
        return $this->helper->responseJson(0, 'رقم الهاتف أو إسم المستخدم غير صحيح');
    }

    //this login method can use in login and register in same time
    public function login(Request $request)
    {
        $rules =
            [
                'user_name' => 'required',
                'phone' => 'required|regex:/(05)[0-9]{8}/|max:10',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $user = $this->model->where(['user_name' => $request->user_name])->first();

        if ($user) {
            if ($user->phone == $request->phone) {
                // send pin code to confirm phone
                return $this->sendPinCode($request, $user);

            } else {
                return $this->helper->responseJson(0, 'رقم الهاتف غير صحيح');
            }
        } else {

            //where user not exists we can create one
            $user = $this->model->create(['phone' => $request->phone, 'user_name' => $request->user_name]);

            // send pin code to confirm phone
            return $this->sendPinCode($request, $user);
        }
    }

    public function updateProfile(Request $request)
    {

        $client = $request->user('api_client');

        $rules =
            [
                'name' => 'nullable',
                'user_name' => 'nullable|unique:clients,user_name,' . $client->id,
                'mute_notification' => 'in:0,1',
                'email' => 'nullable|email|unique:clients,email,' . $client->id,
                'd_o_b' => 'nullable|date',
                'gender' => 'nullable|in:male,female',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first());
        }

        //update user data if not try to update phone
        $client->update($request->all());

        if ($request->hasFile('photo')) {

            Photo::updatePhoto($request->file('photo'), $client->photo, $client, 'clients');
        }

        $client->refresh();

        return $this->helper->responseJson(1, 'SUCCESS', ['token' => $request->bearerToken(), 'user' => new \App\Http\Resources\Client($client)]);

    }

    public function clientSendPinCode(Request $request)
    {
        $rules =
            [
                'user_name' => 'required|exists:clients,user_name',
                'phone' => 'required|exists:clients,phone',
            ];

        $sms =
            [
                'phone.required' => 'الرقم مطلوب',
            ];

        $data = validator()->make($request->all(), $rules, $sms);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->model->where(['phone' => $request->phone, 'user_name' => $request->user_name])->first();

        if ($record) {
            return $this->sendPinCode($request, $record);
        }

        return $this->helper->responseJson(0, 'اسم المستخدم غير صحيح');
    }

    public function resetPasswordOutAuth(Request $request)
    {
        $rules =
            [
                'user_name' => 'required|exists:clients,user_name',
                'phone' => 'required|exists:clients,phone',
                'pin_code' => 'required|numeric',
                'password' => 'required|confirmed|min:6',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->model->where(['phone' => $request->phone, 'user_name' => $request->user_name])->first();

        if ($record) {

            if ($record->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($record->pin_code_date_expired)) {

                    $record->pin_code = $this->getPinCode();
                    $record->password = Hash::make($request->password);
                    $record->save();

                    return $this->helper->responseJson(1, 'تم تغيير كلمة المرور بنجاح');
                }

                return $this->helper->responseJson(0, 'انتهت صلاحية كود التفعيل , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }

        return $this->helper->responseJson(0, 'رقم الهاتف غير صحيح');
    }

}
