<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ViewController extends Controller
{

    public function dataComplete()
    {
        return view('Api.data_complete')->render();
    }

    public function termsAndConditions()
    {
        return view('Api.terms_and_conditions')->render();
    }
}
