<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Notification as NotificationResources;
use App\Http\Resources\NotificationOrder;
use App\Http\Resources\PointsReport;
use App\Models\CharitableActor;
use App\Models\Notification;
use App\Models\Contact;
use App\Models\Delivery;
use App\Models\Setting;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ParentApi extends Controller
{

    public $helper;
    public $guard;
    public $model;
    public $table;
    public $uniqueRow;
    public $sendPinCodeErrorMessage;
    public $apiResource;
    public $pointsCosts = ['api_client' => 'client_points_cost', 'api_delivery' => 'provider_points_cost'];
    public $relations = ['api_client' => 'clients', 'api_delivery' => 'deliveries'];
    public $contactTypes = [
        'api_client' => 'in:complaint,delivery_not_respond',
        'api_delivery' => 'in:complaint'
    ];

    /**
     * @return int
     */
    public function getPinCode(): int
    {
        //TODO rand when confirm rand(1000 , 9999)
        return 123123;
    }

    /**
     * @return string
     */
    public function getPinCodeExpiredDate(): string
    {
        return Carbon::now()->addMinutes(1);
    }

    /**
     * @return string
     */
    public function checkPinCodeExpiredDate($expired_date): string
    {
        $check = Carbon::now() > $expired_date ? false : true;
        return $check;
    }

    public function sendPinCode(Request $request , $model = null)
    {

        // resend pin code in register or login
        $record = $model == null ? $this->model->where($this->uniqueRow, $request[$this->uniqueRow])->first() : $model;

        if ($record) {

            $pin_code = $this->getPinCode();
            $record->pin_code = $pin_code;

            //TODO
//            $this->sendSmsWithCode($record->phone , $pin_code);

            $record->pin_code_date_expired = $this->getPinCodeExpiredDate();
            $record->save();

            return $this->helper->responseJson(1, 'تم ارسال الكود بنجاح');
        }
        return $this->helper->responseJson(0, $this->sendPinCodeErrorMessage);
    }

    public function showProfile(Request $request)
    {
        $user = $request->user($this->guard);

        return $this->helper->responseJson(1, 'تمت العملية', ['token' => $request->bearerToken(), 'user' => new $this->apiResource($user)]);

    }

    public function logOut(Request $request)
    {
        $rules =
            [
                'serial_number' => 'required|exists:tokens,serial_number',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $request->user($this->guard)->tokens()->where('serial_number', $request->serial_number)->delete();

        return $this->helper->responseJson(1, 'تم تسجيل الخروج بنجاح');
    }


    public function resetPasswordOutAuth(Request $request)
    {

        $rules =
            [
                'name' => 'required',
                'pin_code' => 'required|numeric',
                'password' => 'required|confirmed|min:6',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->model->where('user_name', $request->name)->first();

        if ($record) {
            if ($record->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($record->pin_code_date_expired)) {

                    $record->pin_code = $this->getPinCode();
                    $record->password = Hash::make($request->password);
                    $record->save();

                    return $this->helper->responseJson(1, 'تم تغيير كلمة المرور بنجاح');
                }

                return $this->helper->responseJson(0, 'انتهت صلاحية كود التفعيل , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }
        return $this->helper->responseJson(0, 'اسم المستخدم غير صحيح');

    }

    public function resetPasswordInAuth(Request $request)
    {
        $rules =
            [
                'password' => 'required|confirmed|min:6',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = auth($this->guard)->user();
        $record->password = Hash::make($request->password);
        $record->save();

        return $this->helper->responseJson(1, 'عملية ناجحة');

    }

    public function resetPassword(Request $request)
    {
        return auth($this->guard)->user() ? $this->resetPasswordInAuth($request) : $this->resetPasswordOutAuth($request);
    }

    public function acceptPrivacy(Request $request)
    {
        $user = $request->user($this->guard);

        $user->accept_privacy = 1;
        $user->save();

        return $this->helper->responseJson(1, 'SUCCESS', ['token' => $request->bearerToken(), 'user' => new $this->apiResource($user)]);
    }

    /////////////////////////////////////////////

    public function contactUs(Request $request)
    {
        $user = $request->user($this->guard);


        $rules =
            [
                'type' => 'required|'.$this->contactTypes[$this->guard],
                'order_id' => 'required_unless:type,complaint',
                'contact' => 'required',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if($request->input('order_id') && !$user->orders()->find($request->order_id))
        {
            return $this->helper->responseJson(0, 'خطأ في ال order id');
        }

        $contact = $user->contacts()->create($request->all());

        if($request->hasFile('photo'))
            Photo::addPhoto($request->file('photo'),$contact,'contacts', ['size' => 600]);

        if($request->input('order_id') && $this->guard == 'api_client' && $user->orders()->find($request->order_id)->delivery)
        {
            $this->helper->sendNotification($contact, [$user->orders()->find($request->order_id)->delivery->id] , 'deliveries', '#'.$request->order_id.' دليك شكوي بشأن طلب ', '#'.$request->order_id.' قام عميل بتقديم شكوي بشأن طلب  ', 'contact', new \App\Http\Resources\Notification($contact));
        }

        return $this->helper->responseJson(1, 'تم الارسال بنجاح');
    }


    ///////// Notifications //////////////////////////


    public function notifications(Request $request)
    {
        $records = $request->user($this->guard)->notifications()->where(function ($q) use ($request) {

            if ($request->notification_id) {

                $q->where('notification_id', $request->notification_id);
            }

        })->latest()->paginate(20);


        return (NotificationResources::collection($records))->responseTo($this->guard == 'api_delivery' ? 'delivery' : 'client')->additional([
            'status' => 1,
            'massage' => 'تمت العملية',
        ]);
    }

    public function deleteNotification(Request $request)
    {
        $relations = $this->relations[$this->guard];
        $user = $request->user($this->guard);

        $rules =
            [
                'notification_id' => 'required|exists:notifications,id'
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first());
        }

        $notification = $user->notifications()->find($request->notification_id);

        if (!$notification)
            return $this->helper->responseJson(0, 'حدث خطأ ما');

        $notification->$relations()->detach($user->id);

        if (!$notification->clients()->count() || !$notification->deliveries()->count())
            $notification->delete();

        return $this->helper->responseJson(1, 'تم الحذف بنجاح');
    }

    public function readNotification(Request $request)
    {
        $relations = $this->relations[$this->guard];
        $user = $request->user($this->guard);

        $rules =
            [
                'notification_id' => 'required|exists:notifications,id'
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first());
        }

        $notification = $user->notifications()->find($request->notification_id);

        if (!$notification)
            return $this->helper->responseJson(0, 'حدث خطأ ما');

        $notification->$relations()->updateExistingPivot($user->id, ['is_read' => 1]);

        return $this->helper->responseJson(1, 'تمت العملية');
    }

    ///////// ///////////// //////////////////////////

}
