<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\Points as ResourcePoints;
use App\Models\Delivery;
use App\MyHelper\Helper;
use Illuminate\Http\Request;

class PointsController extends ParentApi
{
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_delivery';
        $this->model = new Delivery();
        $this->apiResource = '\App\Http\Resources\Provider\Provider';
        $this->table = 'deliveries';
        $this->uniqueRow = 'user_name';
        $this->sendPinCodeErrorMessage = 'إسم لمستخدم غير صحيح';
    }

    public function listPoints(Request $request)
    {
        $user = $request->user($this->guard);

        $points = $user->points()->where('type','order')->latest()->paginate(15);

        return (ResourcePoints::collection($points))->additional([
            "status" => 1,
            "massage" => "تمت العملية",
            "last_points" =>  $user->last_points,
            "last_points_in_rs" => $user->last_points_in_rs,
            "last_points_expired_date" => $user->last_points_expired_date,

        ]);
    }

    public function changePoints(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'points_number' => 'required|numeric|max:'. $user->last_points,
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first());
        }

        if($user->last_points_expired_date)
        {
            $balance = $request->points_number * Helper::settingValue('points_factor' , 0);

            $user->points()->create([
                'points_before' => $user->last_points,
                'quantity' => - $request->points_number,
                'points_after' => $user->last_points - $request->points_number,
                'factor' => Helper::settingValue('points_factor' , 0),
                'balance' => $balance,
                'type' => 'change',
                'expired_date' => $user->last_points_expired_date_no_format,
            ]);

            return $this->helper->responseJson(1, 'تم التحويل بنجاح');
        }

        return $this->helper->responseJson(0, 'حدث خطأ ما الرجاء المحاولة لاحقا');
    }

}
