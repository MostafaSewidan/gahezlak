<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\Notification;
use App\Http\Resources\NotificationOrder;
use App\Http\Resources\Review as ResourceReview;
use App\Http\Resources\Review;
use App\Models\Delivery;
use App\MyHelper\Helper;
use Illuminate\Http\Request;

class TransactionController extends ParentApi
{
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_delivery';
        $this->model = new Delivery();
        $this->apiResource = '\App\Http\Resources\Provider\Provider';
        $this->table = 'deliveries';
        $this->uniqueRow = 'user_name';
        $this->sendPinCodeErrorMessage = 'إسم لمستخدم غير صحيح';
    }


    ///////////////////////////////////////
    /// ========== reviews cycle ====///

    // list reviews
    public function listReviews(Request $request)
    {
        $user = $request->user($this->guard);

        $reviews = $user->serviceProviderReviews()->latest()->paginate(15);

        return (ResourceReview::collection($reviews))->additional([
            "status" => 1,
            "massage" => "تمت العملية",
            'delivery_rate' => $user->serviceProviderReviews()->count() ? number_format($user->serviceProviderReviews()->avg('rate'), 1) : '0.0',

        ]);
    }

    // add review to client
    public function addReview(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'rate' => 'required|numeric|max:5,min:1',
                'order_id' => 'required|exists:orders,id',
                'comment' => 'required',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereIn('orders.status', ['delivery_handed', 'client_handed'])->find($request->order_id);

        if (!$order)
            return $this->helper->responseJson(0, 'لم يتم العثور علي الطلب');

        if ($order->clientReviews) {

           $review = $order->clientReviews;
           $review->update($request->all());

        } else {

            $request->merge([
                'delivery_id' => $user->id,
                'client_id' => $order->client_id,
            ]);

           $review = $order->clientReviews()->create($request->all());

        }

        $this->helper->sendNotification($review, [$order->client_id], 'clients', 'يوجد لديك تقييم جديد ', ' قام مندوب التوصيل الخاص بالطلب برقم '.'#' . $order->id .' بعمل تقييم لك  ', 'order', new Review($review));

        return $this->helper->responseJson(1, 'تم عمل التقييم بنجاح');
    }
    /// //////////////////////////////////////

}
