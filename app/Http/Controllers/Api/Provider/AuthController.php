<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Api\ParentApi;
use App\Models\Client;
use App\Models\Delivery;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;

class AuthController extends ParentApi
{
//////////////////////////////
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_delivery';
        $this->model = new Delivery();
        $this->apiResource = '\App\Http\Resources\Provider\Provider';
        $this->table = 'deliveries';
        $this->uniqueRow = 'user_name';
        $this->sendPinCodeErrorMessage = 'إسم لمستخدم غير صحيح';
    }

    //this login method can use in login and register in same time
    public function login(Request $request)
    {
        $rules =
            [
                'user_name' => 'required',
                'password' => 'required|min:6',
                'accept_privacy' => 'required|in:1',
                'token' => 'required',
                'serial_number' => 'required',
                'os' => 'required|in:android,ios',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $user = $this->model->where(['user_name' => $request->user_name])->first();

        //check if user exists
        if ($user) {
            if (Hash::check($request->password, $user->password)) {

                // check user confirmation and activation
                ///
                if (in_array($user->status,['pending'])) {

                    return $this->helper->responseJson(0, 'لم يتم تاكيد الحساب بعد الرجاء إنتظار موافقة الإدارة');

                } elseif ($user->status == 'rejected') {
                    return $this->helper->responseJson(0, 'تم رفض طلبك للانضمام كمندوب توصيل');

                } elseif ($user->status == 'deactivate') {

                    return $this->helper->responseJson(0, 'تم حظرك من الاستخدام يمكنك التواصل مع الادارة');
                }

                $user->accept_privacy = $request->accept_privacy;
                $user->save();
                ///
                ///////////////////

                //create passport token
                $token = $user->createToken('android')->accessToken;

                if ($user->tokens()->where('serial_number', $request->serial_number)->first()) {
                    $phone_token = $user->tokens()->where('serial_number', $request->serial_number)->first();
                    $phone_token->update(
                        [
                            'token' => $request->token,
                            'phone_type' => $request->os,
                            'serial_number' => $request->serial_number
                        ]);

                } else {

                    $user->tokens()->create(['token' => $request->token, 'phone_type' => $request->os, 'serial_number' => $request->serial_number]);
                }

                return $this->helper->responseJson(1, 'تم تسجيل الدخول بنجاح', ['token' => $token, 'user' => new $this->apiResource($user)]);


            }
            return $this->helper->responseJson(0,'كلمة المرور غير صحيحة');
        }

        return $this->helper->responseJson(0,'إسم المستخدم غير صحيح');
    }

    public function updateProfile(Request $request)
    {

        $user = $request->user($this->guard);

        $rules =
            [
                'name' => 'nullable',
                'user_name' => 'nullable|unique:clients,user_name,'.$user->id,
                'mute_notification' => 'nullable|in:0,1',
                'email' => 'nullable|email|unique:clients,email,'.$user->id,
                'personal_photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first());
        }

        //update user data if not try to update phone
        $user->update([
            'name' => $request->input('name') ? $request->name : $user->name ,
            'user_name' => $request->input('user_name') ? $request->user_name : $user->user_name,
            'mute_notification' => $request->mute_notification != null ? $request->mute_notification : $user->mute_notification,
            'email' => $request->input('email') ? $request->email : $user->email,
        ]);

        if ($request->hasFile('personal_photo')) {

            Photo::updatePhoto(
                $request->file('personal_photo'),
                $user->photos()->where('usage', 'personal')->first(),
                $user,
                'deliveries/personal',
                ['size' => 600,'relation' => 'photos' , 'usage' => 'personal']
            );
        }

        $user->refresh();

        return $this->helper->responseJson(1, 'SUCCESS', ['token' => $request->bearerToken(), 'user' => new $this->apiResource($user)]);

    }

    public function resetPasswordOutAuth(Request $request)
    {
        $rules =
            [
                'user_name' => 'required|exists:deliveries,user_name',
                'pin_code' => 'required|numeric',
                'password' => 'required|confirmed|min:6',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->model->where(['user_name' => $request->user_name])->first();

        if ($record) {

            if ($record->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($record->pin_code_date_expired)) {

                    $record->pin_code = null;
                    $record->password = Hash::make($request->password);
                    $record->save();

                    return $this->helper->responseJson(1, 'تم تغيير كلمة المرور بنجاح');
                }

                return $this->helper->responseJson(0, 'انتهت صلاحية كود التفعيل , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }

        return $this->helper->responseJson(0, 'رقم الهاتف غير صحيح');
    }

    public function register(Request $request)
    {
        $rules =
            [
                'user_name' => 'required|unique:deliveries,user_name',
                'name' => 'required',
                'phone' => 'required|regex:/(05)[0-9]{8}/|unique:deliveries,phone',
                'email' => 'required|email|unique:deliveries,email',
                'id_card_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'personal_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'driving_license_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'form_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'front_car_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'back_car_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'criminal_record_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->model->create($request->all());


        Photo::addPhoto(
            $request->file('id_card_photo'),
            $record,
            'deliveries/id_card',
            ['size' => 600,'relation' => 'photos' , 'usage' => 'id_card']
        );

        Photo::addPhoto(
            $request->file('personal_photo'),
            $record,
            'deliveries/personal',
            ['size' => 600,'relation' => 'photos' , 'usage' => 'personal']
        );

        Photo::addPhoto(
            $request->file('driving_license_photo'),
            $record,
            'deliveries/driving_license',
            ['size' => 600,'relation' => 'photos' , 'usage' => 'driving_license']
        );

        Photo::addPhoto(
            $request->file('form_photo'),
            $record,
            'deliveries/form',
            ['size' => 600,'relation' => 'photos' , 'usage' => 'form']
        );

        Photo::addPhoto(
            $request->file('front_car_photo'),
            $record,
            'deliveries/front_car',
            ['size' => 600,'relation' => 'photos' , 'usage' => 'front_car']
        );

        Photo::addPhoto(
            $request->file('back_car_photo'),
            $record,
            'deliveries/back_car',
            ['size' => 600,'relation' => 'photos' , 'usage' => 'back_car']
        );

        Photo::addPhoto(
            $request->file('criminal_record_photo'),
            $record,
            'deliveries/criminal_record',
            ['size' => 600,'relation' => 'photos' , 'usage' => 'criminal_record']
        );

        return $this->helper->responseJson(1, 'تم التقدم بالطلب للإدارة , الرجاء انتظار التفعيل');
    }

    public function updateLocation(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'latitude' => 'required',
                'longitude' => 'required',
            ];


        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->is_set_location = 1;
        $user->save();

        return $this->helper->responseJson(1, 'تم التحديث بنجاح');
    }

}
