<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\DeliveryOffer;
use App\Http\Resources\NotificationDeliveryOffer;
use App\Http\Resources\NotificationOrder;
use App\Http\Resources\Order as ResourceOrder;
use App\Models\Delivery;
use App\Models\Order;
use App\MyHelper\Helper;
use Illuminate\Http\Request;

class OrderController extends ParentApi
{
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_delivery';
        $this->model = new Delivery();
        $this->apiResource = '\App\Http\Resources\Provider\Provider';
        $this->table = 'deliveries';
        $this->uniqueRow = 'user_name';
        $this->sendPinCodeErrorMessage = 'إسم لمستخدم غير صحيح';
    }

    public function listOrders(Request $request)
    {
        $user = $request->user($this->guard);

        $orders = $user->pendingOrders()->latest()->paginate(15);

        return (ResourceOrder::collection($orders)->responseTo('delivery'))->additional([
            "status" => 1,
            "massage" => "تمت العملية",
        ]);
    }

    public function offerOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
                'price' => 'required',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first());
        }

        $order = $user->pendingOrders()->find($request->order_id);

        if (!$order) {

            return $this->helper->responseJson(0, 'لم يتم العثور علي الطلب ');
        }

        $distanceTimes = Order::distanceTimes([

            'client_latitude' => $order->client_latitude,
            'client_longitude' => $order->client_longitude,
            'target_latitude' => $order->target_latitude,
            'target_longitude' => $order->target_longitude,
            'delivery_latitude' => $user->latitude,
            'delivery_longitude' => $user->longitude,
        ]);

        $order->deliveries()->updateExistingPivot($user->id, [
            'price' => $request->price,
            'status' => 'offer',
            'deliver_expected_time' => $distanceTimes['deliver_expected_time'],
            'minuets_far_target_location' => $distanceTimes['minuets_far_target_location'],
        ]);

        $order->refresh();


        $this->helper->sendNotification(
            $order,
            [$order->client_id] ,
            'clients',
            $order->id .' بتوصيل طلبك '.optional($order->client)->name .'هناك عرض من  ',
            ' , إذا كان العرض يعجبك الرجاء الموافقة , جاهزلك يتشرف بخدمتك دائما '.$order->id.' مندوب جاهزلك بعرض توصيل طلبك برقم ' . optional($order->client)->name .' قام ',
            'order_offer', new NotificationDeliveryOffer(optional($user->offerOrders()->find($request->order_id)))
        );

        return $this->helper->responseJson(1, 'تم إرسال العرض للعميل , في انتظار الموافقة عليه ',new DeliveryOffer(optional($user->offerOrders()->find($request->order_id))));
    }

}
