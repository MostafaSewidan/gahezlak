<?php

namespace App\Http\Middleware;


use App\Models\Client;
use App\Models\Delivery;
use App\Models\Store;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Closure;
use Illuminate\Auth\AuthenticationException;


class LastActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $guard = null;

        if(auth()->check())
        {
           if(auth()->user() instanceof Client)
           {
               $guard = 'api_client';

           }elseif(auth()->user() instanceof Delivery)
           {
               $guard = 'api_delivery';
           }
        }

        if($guard != null)
        {
            $user = auth($guard)->user();
            $user->last_login = Carbon::now();
            $user->save();
        }

        return $next($request);
    }
}
