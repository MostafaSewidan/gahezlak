<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryTransaction extends Model 
{

    protected $table = 'delivery_transactions';
    public $timestamps = true;
    protected $fillable = array('delivery_id');

}