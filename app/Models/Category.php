<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'parent_id', 'order');
    protected $appends = array('icon', 'photo');

    public function stores()
    {
        return $this->belongsToMany('App\Models\Store');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category','parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category','parent_id');
    }

    public function photos()
    {
        return $this->morphMany(Attachment::class , 'attachmentable');
    }

    public function getPhotoAttribute()
    {
        return $this->photos()->where('usage', null)->first() ? $this->photos()->where('usage', null)->first()->path : null;
    }

    public function getIconAttribute()
    {
        return $this->photos()->where('usage', 'icon')->first() ? $this->photos()->where('usage', 'icon')->first()->path : null;
    }
}