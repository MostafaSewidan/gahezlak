<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model 
{

    protected $table = 'credit_cards';
    public $timestamps = true;
    protected $fillable = array('type');

    public function cardable()
    {
        return $this->morphTo();
    }

}