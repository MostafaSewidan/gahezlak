<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model 
{

    protected $table = 'contacts';
    public $timestamps = true;
    protected $fillable = array('order_id', 'contact' ,'type', 'contactable_type', 'contactable_id');

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function contactable()
    {
        return $this->morphTo();
    }

    public function photo()
    {
        return $this->morphOne(Attachment::class, 'attachmentable');
    }

    public function notifications()
    {
        return $this->morphMany('App\Models\Notification', 'notifiable');
    }
}