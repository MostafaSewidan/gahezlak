<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreAddress extends Model
{

    protected $table = 'store_addresses';
    public $timestamps = true;
    protected $fillable = array('store_id', 'title', 'type', 'latitude', 'longitude');

    public function store()
    {
        return $this->belongsTo('App\Models\Store');
    }

}