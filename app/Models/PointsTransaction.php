<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointsTransaction extends Model 
{

    protected $table = 'points_transactions';
    public $timestamps = true;
    protected $fillable = array('delivery_id', 'order_id', 'points_before', 'quantity', 'points_after', 'factor', 'balance','type','expired_date');
    protected $dates = ['expired_date'];

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}