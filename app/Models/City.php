<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model 
{

    protected $table = 'cities';
    public $timestamps = true;

    public function ordersAsStart()
    {
        return $this->hasMany('App\Models\Order', 'start_city_id');
    }

    public function ordersAsEnd()
    {
        return $this->hasMany('App\Models\Order', 'end_city_id');
    }

    public function regions()
    {
        return $this->hasMany(Region::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

}