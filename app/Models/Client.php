<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Client extends Authenticatable
{

    use HasApiTokens, Notifiable;

    protected $table = 'clients';
    public $timestamps = true;
    protected $fillable = array('user_name', 'name', 'email', 'phone', 'gender', 'd_o_b', 'activation', 'accept_privacy', 'mute_notification', 'last_login');

    public function contacts()
    {
        return $this->morphMany('App\Models\Contact', 'contactable');
    }

    public function credit_cards()
    {
        return $this->morphMany('App\Models\CreditCard', 'cardable');
    }

    public function clientReviews()
    {
        return $this->hasMany('App\Models\ClientReview');
    }

    public function serviceProviderReviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function photo()
    {
        return $this->morphOne(Attachment::class , 'attachmentable');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'tokenable');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function notifications()
    {
        return $this->morphToMany('App\Models\Notification', 'notifiable')->withPivot('is_read');
    }
}