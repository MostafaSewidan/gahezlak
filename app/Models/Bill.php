<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model 
{

    protected $table = 'bills';
    public $timestamps = true;
    protected $fillable = array('order_id', 'description', 'price');

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

}