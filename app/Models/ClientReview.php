<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientReview extends Model 
{

    protected $table = 'client_reviews';
    public $timestamps = true;
    protected $fillable = array('client_id', 'delivery_id', 'rate', 'comment', 'order_id');

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function delivery()
    {
        return $this->belongsTo('App\Models\Delivery');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function notifications()
    {
        return $this->morphMany('App\Models\Notification', 'notifiable');
    }

}