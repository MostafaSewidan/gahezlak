<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDescription extends Model 
{

    protected $table = 'service_descriptions';
    public $timestamps = true;
    protected $fillable = array('service_id', 'description');

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

}