<?php

namespace App\Models;

use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Delivery extends Authenticatable
{

    use HasApiTokens, Notifiable;

    protected $table = 'deliveries';
    public $timestamps = true;
    protected $fillable = array('user_name', 'name', 'phone', 'email', 'status', 'last_login', 'latitude', 'longitude', 'mute_notification');

    public function contacts()
    {
        return $this->morphMany('App\Models\Contact', 'contactable');
    }

    public function credit_cards()
    {
        return $this->morphMany('App\Models\CreditCard', 'cardable');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Models\Order')->withPivot('price', 'status', 'deliver_expected_time', 'minuets_far_target_location');;
    }

    public function pendingOrders()
    {
        return $this->belongsToMany('App\Models\Order')
            ->where('delivery_order.status' , 'pending')
            ->where('orders.status' , 'pending')
            ->withPivot('price', 'status', 'deliver_expected_time', 'minuets_far_target_location');
    }

    public function offerOrders()
    {
        return $this->belongsToMany('App\Models\Order')
            ->where('delivery_order.status' , 'offer')
            ->where('orders.status' , 'pending')
            ->withPivot('price', 'status', 'deliver_expected_time', 'minuets_far_target_location');
    }

    public function acceptedOrders()
    {
        return $this->belongsToMany('App\Models\Order')
            ->where('delivery_order.status' , 'accepted')
            ->where('orders.status' , 'accepted')
            ->withPivot('price', 'status', 'deliver_expected_time', 'minuets_far_target_location');
    }

    public function rejectedOrders()
    {
        return $this->belongsToMany('App\Models\Order')
            ->where(function ($q) {

                $q->where('delivery_order.status' , 'rejected')
                    ->orWhere('orders.status' , 'rejected');

            })
            ->withPivot('price', 'status', 'deliver_expected_time', 'minuets_far_target_location');
    }

    public function canceledOrders()
    {
        return $this->belongsToMany('App\Models\Order')
            ->where(function ($q) {

                $q->where('delivery_order.status' , 'canceled')
                    ->orWhere('orders.status' , 'canceled');

            })
            ->withPivot('price', 'status', 'deliver_expected_time', 'minuets_far_target_location');
    }

    public function clientReviews()
    {
        return $this->hasMany('App\Models\ClientReview');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'tokenable');
    }

    public function points()
    {
        return $this->hasMany(PointsTransaction::class);
    }

    public function paymentMethods()
    {
        return $this->hasMany(DeliveryPaymentMethod::class);
    }

    public function getLastPointsAttribute()
    {
        return $this->points()->latest()->first() && !$this->points()->latest()->first()->expired_date->isPast() ?
            $this->points()->latest()->first()->points_after : 0;
    }

    public function getLastPointsInRsAttribute()
    {
        return $this->points()->latest()->first() && !$this->points()->latest()->first()->expired_date->isPast() ?
            $this->points()->latest()->first()->points_after * Helper::settingValue('points_factor' , 0) : 0;
    }

    public function getLastPointsExpiredDateAttribute()
    {
        return $this->points()->latest()->first() && !$this->points()->latest()->first()->expired_date->isPast() ?
            Carbon::parse($this->points()->latest()->first()->expired_date)->format('d-m-Y') : null;
    }

    public function getLastPointsExpiredDateNoFormatAttribute()
    {
        return $this->points()->latest()->first() && !$this->points()->latest()->first()->expired_date->isPast() ?
            $this->points()->latest()->first()->expired_date : null;
    }

    public function serviceProviderReviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service');
    }

    public function photos()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function getIdCardPhotoAttribute()
    {
        return $this->photos()->where('usage', 'id_card')->first() ? asset($this->photos()->where('usage', 'id_card')->first()->path) : null;
    }

    public function getPersonalPhotoAttribute()
    {
        return $this->photos()->where('usage', 'personal')->first() ? asset($this->photos()->where('usage', 'personal')->first()->path) : null;
    }

    public function getDrivingLicensePhotoAttribute()
    {
        return $this->photos()->where('usage', 'driving_license')->first() ? asset($this->photos()->where('usage', 'driving_license')->first()->path) : null;
    }

    public function getFormPhotoAttribute()
    {
        return $this->photos()->where('usage', 'form')->first() ? asset($this->photos()->where('usage', 'form')->first()->path) : null;
    }

    public function getFrontCarPhotoAttribute()
    {
        return $this->photos()->where('usage', 'front_car')->first() ? asset($this->photos()->where('usage', 'front_car')->first()->path) : null;
    }

    public function getBackCarPhotoAttribute()
    {
        return $this->photos()->where('usage', 'back_car')->first() ? asset($this->photos()->where('usage', 'back_car')->first()->path) : null;
    }

    public function getCriminalRecordPhotoAttribute()
    {
        return $this->photos()->where('usage', 'criminal_record')->first() ? asset($this->photos()->where('usage', 'criminal_record')->first()->path) : null;
    }

    /////////////////////////////////////
    ///-------- scopes  -------------///

    public function scopeactivation($query)
    {
        return $query->where('status', 'active')->where('is_set_location', 1);
    }

    public function scopehasService($query, $serviceId)
    {
        return $query->whereHas('services', function ($q) use ($serviceId) {
            $q->where('service_id', $serviceId);
        });
    }

    public function scopehasPayment($query, $payment)
    {
        return $query->whereHas('paymentMethods', function ($q) use ($payment) {
            $q->where('payment_method', $payment);
        });
    }

    static function CheckArea($points , $options)
    {
        $distance = Helper::inArray('distance', $options, 0);
        $serviceId = Helper::inArray('service_id', $options, null);
        $paymentMethod = Helper::inArray('payment_method', $options, Order::$paymentMethods[0]);

        if($serviceId != null)
        {
            return Delivery::activation()
                ->hasService($serviceId)
                ->hasPayment($paymentMethod)
                ->selectRaw('*, ( 6367 * acos( cos( radians(' . $points['latitude'] . ') ) * cos( radians( latitude ) ) * 
                    cos( radians( longitude ) - radians(' . $points['longitude'] . ') ) + sin( radians(' . $points['latitude'] . ') ) * sin( radians( latitude ) ) ) ) AS distance')
                ->having('distance','<',$distance)->take(50)->pluck('id')->toArray();

        }else{

            return Delivery::activation()
                ->hasPayment($paymentMethod)
                ->selectRaw('*, ( 6367 * acos( cos( radians(' . $points['latitude'] . ') ) * cos( radians( latitude ) ) * 
                    cos( radians( longitude ) - radians(' . $points['longitude'] . ') ) + sin( radians(' . $points['latitude'] . ') ) * sin( radians( latitude ) ) ) ) AS distance')
                ->having('distance','<',$distance)->take(50)->pluck('id')->toArray();
        }
    }

    ////////////////////////////////////////
}