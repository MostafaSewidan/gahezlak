<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model 
{

    protected $table = 'services';
    public $timestamps = true;
    protected $fillable = array('title', 'parent_id', 'type','search_area');
    protected $appends = ['image'];

    public function descriptions()
    {
        return $this->hasMany('App\Models\ServiceDescription');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Service', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Service', 'parent_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'service_id');
    }

    public function subServiceOrders()
    {
        return $this->hasMany('App\Models\Order', 'sub_service_id');
    }

    public function photo()
    {
        return $this->morphOne(Attachment::class , 'attachmentable');
    }

    public function getImageAttribute()
    {
        return $this->photo ? asset($this->photo->path) : null;
    }

}