<?php

namespace App\Models;

use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';
    public static $paymentMethods = ['cache' , 'card'];
    public $timestamps = true;
    protected $fillable = array(
        'service_id',
        'sub_service_id',
        'type',
        'store_id',
        'start_city_id',
        'end_city_id',
        'client_latitude',
        'client_longitude',
        'target_latitude',
        'target_longitude',
        'handed_name',
        'phone',
        'status',
        'delivery_term',
        'days_number',
        'payment_method',
        'description',
        'for_other_one',
        'delivery_price',
        'bill_price',
        'coupon_cut',
        'net',
        'commission_status',
        'commission',
        'commission_present',
        'delivery_cancel_expired_date',
        'natunalID'
    );

    protected $dates = ['created_at', 'updated_at'];

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function bill()
    {
        return $this->hasMany('App\Models\Bill');
    }

    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }

    public function subService()
    {
        return $this->belongsTo('App\Models\Service', 'sub_service_id');
    }

    public function deliveries()
    {
        return $this->belongsToMany('App\Models\Delivery')->withPivot('price', 'status', 'deliver_expected_time', 'minuets_far_target_location');
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function points()
    {
        return $this->hasOne(PointsTransaction::class);
    }

    public function clientReviews()
    {
        return $this->hasOne('App\Models\ClientReview');
    }

    public function serviceProviderReviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function notifications()
    {
        return $this->morphMany('App\Models\Notification', 'notifiable');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store');
    }

    public function startCity()
    {
        return $this->belongsTo('App\Models\City', 'start_city_id');
    }

    public function endCity()
    {
        return $this->belongsTo('App\Models\City', 'end_city_id');
    }

    public function photos()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function getDescriptionPhotoAttribute()
    {
        return $this->photos()->where('usage', null)->first() ? asset($this->photos()->where('usage', null)->first()->path) : null;
    }

    public function getBillPhotoAttribute()
    {
        return $this->photos()->where('usage', 'bill')->first() ? asset($this->photos()->where('usage', 'bill')->first()->path) : null;
    }

    public function getClientStatusTextAttribute()
    {
        $status_text = '';

        switch ($this->status) {
            case 'pending':
                $status_text = 'طلب جديد';
                break;
            case 'accepted':
                $status_text = 'تم قبول الطلب';
                break;
            case 'delivery_load':
                $status_text = 'المندوم إستلم الطلب';
                break;
            case 'arrived_to_client':
                $status_text = 'المندوب وصل للعميل';
                break;
            case 'delivery_handed':
                $status_text = 'قام المندوب بتسليم الطلب';
                break;
            case 'client_handed':
                $status_text = 'تم الإستلام';
                break;
            case 'rejected':
                $status_text = 'طلب مرفوض';
                break;
            case 'canceled':
                $status_text = 'طلب ملغي';
                break;
        }

        return $status_text;
    }

    public function getProviderStatusTextAttribute()
    {
        $status_text = '';

        switch ($this->status) {
            case 'pending':
                $status_text = 'رحلة جديد';
                break;
            case 'accepted':
                $status_text = 'قبول الرحلة';
                break;
            case 'arrives_to_patient':
                $status_text = 'وصول للحالة';
                break;
            case 'move_with_patient':
                $status_text = 'تحرك بالحالة';
                break;
            case 'arrived_to_hospital':
                $status_text = 'وصول للمستشفي';
                break;
            case 'End_journey':
                $status_text = 'إنهاء الرحلة';
                break;
            case 'cancel':
                $status_text = 'رحلة ملغية';
                break;
            case 'rejected':
                $status_text = 'رحلة مرفوضة';
                break;

            // written orders status
            case 'arrives_to_pharmacy':
                $status_text = 'وصول للصيدلية';
                break;
            case 'delivery_loaded':
                $status_text = 'إستلام الطلب';
                break;
            case 'delivery_go':
                $status_text = 'التحرك للعميل';
                break;
            case 'arrived_to_client':
                $status_text = 'وصلت للعميل';
                break;
        }

        return $status_text;
    }

    public function getDeliveryAttribute()
    {
        if ($this->deliveries()->where('deliveries.status', 'accepted')->first()) {
            return $this->deliveries()->where('deliveries.status', 'accepted')->first();
        } else {
            return null;
        }

    }

    public function getOrderedAfterAttribute()
    {
        return $this->created_at->diffInMinutes(Carbon::now());

    }

    // distance
    public static function distances($locations)
    {
        // client latitude
        // client long
        // target latitude
        // target longitude
        // delivery latitude
        // delivery long

        $client_latitude = Helper::inArray('client_latitude', $locations, null);
        $client_longitude = Helper::inArray('client_longitude', $locations, null);
        $target_latitude = Helper::inArray('target_latitude', $locations, null);
        $target_longitude = Helper::inArray('target_longitude', $locations, null);
        $delivery_latitude = Helper::inArray('delivery_latitude', $locations, null);
        $delivery_longitude = Helper::inArray('delivery_longitude', $locations, null);


        if (
            $client_latitude != null &&
            $client_longitude != null &&
            $target_latitude != null &&
            $target_longitude != null
        ) {

            $type = 'with_target';
            $from_delivery_to_target_location = Helper::distance($delivery_latitude, $delivery_longitude, $target_latitude, $target_longitude);
            $from_target_to_client_location = Helper::distance($target_latitude, $target_longitude, $client_latitude, $client_longitude);
            $from_delivery_to_client_location = $from_target_to_client_location + $from_delivery_to_target_location;

        } elseif (
            $client_latitude != null &&
            $client_longitude != null &&
            $target_latitude == null &&
            $target_longitude == null
        ) {

            $type = 'with_out_target';
            $from_delivery_to_target_location = null;
            $from_target_to_client_location = null;
            $from_delivery_to_client_location = Helper::distance($delivery_latitude, $delivery_longitude, $client_latitude, $client_longitude);

        }else{

            $type = 'with_out_location';
            $from_delivery_to_target_location = null;
            $from_target_to_client_location = null;
            $from_delivery_to_client_location = null;
        }

        $response = [

            'type' => $type,
            'from_delivery_to_target_location' => number_format($from_delivery_to_target_location , 1),
            'from_target_to_client_location' => number_format($from_target_to_client_location , 1),
            'from_delivery_to_client_location' => number_format($from_delivery_to_client_location , 1),
        ];

        return $response;
    }

    // distance times
    public static function distanceTimes($locations)
    {

        $distances = self::distances($locations);

        if($distances['type'] == 'with_target')
        {
            $far = self::getTime($distances['from_delivery_to_target_location']);
            $deliveryTime = self::getTime($distances['from_target_to_client_location']);

        }elseif ($distances['type'] == 'with_out_target')
        {
            $far = self::getTime($distances['from_delivery_to_client_location']);
            $deliveryTime = self::getTime($distances['from_delivery_to_client_location']);

        }else{

            $far = null;
            $deliveryTime = null;
        }

        $response = [

            'type' => $distances['type'],
            'deliver_expected_time' => number_format($deliveryTime,1),
            'minuets_far_target_location' => number_format($far,1),
        ];

        return $response;
    }

    // distance times
    public static function getTime($distance)
    {
        return (($distance  * 60)/ 50) * 1.5;
    }

}