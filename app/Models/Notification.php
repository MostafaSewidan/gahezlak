<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model 
{
    protected $table = 'notifications';
    public $timestamps = true;
    protected $fillable = array('notifiable_type', 'notifiable_id','title','body');
    protected $appends = array('type','resources');

    public function notifiable()
    {
        return $this->morphTo();
    }

    public function clients()
    {
        return $this->morphedByMany('App\Models\Client','notifiable')->withPivot('is_read');
    }

    public function deliveries()
    {
        return $this->morphedByMany(Delivery::class ,'notifiable')->withPivot('is_read');
    }

    public function getTypeAttribute()
    {
        $type = '';

        switch ($this->notifiable_type){
            case 'App\Models\Order':
                $type = 'order';
                break;
            case 'App\Models\ClientReview':
                $type = 'client_review';
                break;
            case 'App\Models\Review':
                $type = 'review';
                break;
            case 'App\Models\DeliveryOrder':
                $type = 'order_offer';
                break;
        }
        return $type;
    }

    public function getResourcesAttribute()
    {
        $value = '';

        switch ($this->notifiable_type){
            case 'App\Models\Order':
                $value = 'App\Http\Resources\Order';
                break;

            case 'App\Models\Review':
                $value = 'App\Http\Resources\Review';
                break;

            case 'App\Models\ClientReview':
                $value = 'App\Http\Resources\ClientReview';
                break;

            case 'App\Models\DeliveryOrder':
                $value = 'App\Http\Resources\DeliveryOrder';
                break;
        }
        return $value;
    }
}