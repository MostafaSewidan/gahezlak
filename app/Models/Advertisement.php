<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model 
{

    protected $table = 'advertisements';
    public $timestamps = true;
    protected $fillable = array('title', 'addable_type', 'addable_id');


    public function photo()
    {
        return $this->morphOne(Attachment::class , 'attachmentable');
    }
}