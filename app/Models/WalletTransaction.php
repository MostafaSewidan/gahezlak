<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model 
{

    protected $table = 'wallet_transactions';
    public $timestamps = true;
    protected $fillable = array('delivery_id');

}