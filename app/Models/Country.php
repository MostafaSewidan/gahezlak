<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model 
{

    protected $table = 'countries';
    public $timestamps = true;

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}