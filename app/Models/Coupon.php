<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model 
{

    protected $table = 'coupons';
    public $timestamps = true;
    protected $fillable = array('code', 'use_frequency', 'expired_date', 'start_date', 'amount', 'type', 'is_active', 'max_discount');

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

}