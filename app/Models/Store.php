<?php

namespace App\Models;

use App\MyHelper\Helper;
use Illuminate\Database\Eloquent\Model;

class Store extends Model 
{

    protected $table = 'stores';
    public $timestamps = true;
    protected $fillable = array('category_id', 'phone', 'name', 'proposed', 'is_active');

    public function addresses()
    {
        return $this->hasMany('App\Models\StoreAddress');
    }

    public function reviews()
    {
        return $this->morphMany('App\Models\Review','reviewable');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function scopeCheckArea($query, $points)
    {
        $area = Helper::settingValue('store_search_area' , 0);

        return $query->whereHas('addresses', function ($q) use ($area, $points) {
            $select = '*, ( 6367 * acos( cos( radians(' . $points['latitude'] . ') ) * cos( radians( latitude ) ) * 
                    cos( radians( longitude ) - radians(' . $points['longitude'] . ') ) + sin( radians(' . $points['latitude'] . ') ) * sin( radians( latitude ) ) ) ) AS distance';

            $q->select(\DB::raw($select))->having('distance', '<', $area)->orderBy('distance');

        });
    }

    public function scopeactivation($query)
    {
        return $query->where('is_active', 1);
    }

    public function scopeproposed($query)
    {
        return $query->where('proposed', 1);
    }

}