<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryPaymentMethod extends Model
{

    protected $table = 'delivery_payment_methods';
    public $timestamps = true;
    protected $fillable = array('delivery_id', 'payment_method');

    public function delivery()
    {
        return $this->belongsTo('App\Models\Delivery');
    }
}