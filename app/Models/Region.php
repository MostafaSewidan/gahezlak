<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model 
{

    protected $table = 'regions';
    public $timestamps = true;

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}