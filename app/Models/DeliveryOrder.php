<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model 
{

    protected $table = 'delivery_order';
    public $timestamps = true;
    protected $fillable = array('order_id', 'delivery_id', 'price', 'status', 'deliver_expected_time', 'minuets_far_target_location');


    public function notifications()
    {
        return $this->morphMany('App\Models\Notification', 'notifiable');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function delivery()
    {
        return $this->belongsTo('App\Models\Delivery');
    }
}