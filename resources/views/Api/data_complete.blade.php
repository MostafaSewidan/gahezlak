@extends('Api.layouts.app')

@section('content')

    @push('style')
        <style>
            body{

                text-align: center;
            }
        </style>
    @endpush
    <p style="color: #F4AA17; font-weight: 900;">للإستفادة من بعض العروض والمزايا والهدايا الخاصة</p>
    <p style="color:#07298B;font-weight: 900;">
        <span>الرجاء </span>
        <span style="border-bottom: 2px solid #07298b91;">إكمال بيناتك من هنا </span>
        <span>أو من الإعدادات لاحقا</span>
    </p>

@endsection
