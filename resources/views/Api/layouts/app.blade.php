<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css2?family=Almarai:wght@300&display=swap" rel="stylesheet">

    <style>
        body{
            font-family: 'Almarai', sans-serif !important;
        }
    </style>
    @stack('style')
</head>
<body dir="rtl">

    @yield('content')

</body>
</html>
